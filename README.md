# Numérique et Sciences Informatiques - Première

Les cours de NSI de première du lycée l'Emperi 2023-2024 sont déposés et générés ici.


Site de rendu du dépôt : [https://bruno.forge.aeif.fr/numerique-et-sciences-informatiques-premiere/](https://bruno.forge.aeif.fr/numerique-et-sciences-informatiques-premiere/)