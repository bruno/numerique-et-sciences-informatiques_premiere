---
author: Bruno Bourgine
title: Algorithmes de tri
hide:
  - footer
---


## **Pourquoi trie-t-on ?**

Dans le précédent chapitre nous avons vu à quel point les recherches de valeurs 
dans un tableau, ainsi que  les comparaisons de tableaux sont beaucoup plus 
efficaces dès lors que ceux-ci sont triés.

En première approche nous allons considérer l'efficacité d'un algorithme en 
terme de nombre d'opérations à effectuer en fonction de la taille des données 
traitées. Le terme utilisé pour décrire l'efficacité d'un algorithme est la 
**complexité**.

Il y a plusieurs types de complexités étudiables (nombre d'opérations, temps nécessaire, 
espace-mémoire nécessaire...).


## 1. Tri par sélection

### 1.1 Principe du tri par sélection

![illustration tri sélection](res/selection.gif)

Le tri par sélection parcourt le tableau de la gauche vers la droite, la partie 
à gauche du tableau étant déja triée et à sa place définitive.

À chaque étape, on cherche le plus petit élément dans la partie non triée du 
tableau, puis on l’échange avec l’élément le plus à gauche de cette partie.

Ainsi, la première étape va sélectionner le plus petit élément et le placer tout
à gauche du tableau. Puis la deuxiéme étape va sélectionner le deuxième plus
petit élément et le placer dans la deuxième case du tableau, et ainsi de suite.


### 1.2 Algorithme du tri par sélection

Le travail se fait essentiellement sur les indices.

        
Du premier élément jusqu'à l'avant-dernier : 

  - on considère que cet élément est l'élément minimum, on stocke donc son indice dans une variable indice du minimum.  
  - on parcourt les éléments suivants, et si on repère un élémént plus petit que notre mininum on met à jour notre indice du minimum.
  - une fois le parcours fini, on échange l'élément de travail avec l'élément minimum qui a été trouvé.

!!! warning "Algorithme en pseudo-code"

    ```linenums="1"
    fonction TRI_SELECTION(tab : Tableau)
        taille  ← longueur(tab)
        Pour i de 0 à taille - 1 , faire
            mini ← i
            Pour j de i+1 à taille, faire
                Si tab[j] < tab[mini] , alors  mini  ← j
            Fin Pour
            temp ← tab[i]
            tab[i] ← tab[mini]
            tab[mini] ← temp
        Fin Pour
    Fin fonction
    ```

!!! example "Implémentation en Python"

    L'implémentation est présentée dans l'IDE ci-dessous, vous pouvez la
    tester avec quelques tableaux.

    {{IDEv('res/tri_selection')}}

### 1.3 Complexité de l'algorithme de tri par sélection

Supposons que la longueur du tableau est $n$.

La première boucle (ligne 3) va donc tourner toujours $n$ fois . 

À chaque tour de la première boucle (qui en comporte $n$), la 2ème boucle 
(ligne 5) va s'exécuter pour des valeurs de $j$ variant :

- la 1<sup>ère</sup> fois de $1$ à $n-1$ : ce qui provoquera de l'ordre de $n-1$ opérations.
- la 2<sup>ème</sup> fois de $2$ à $n-1$ : ce qui provoquera de l'ordre de $n-2$ opérations.
- ...
- la $n$-ième fois de $n-1$ à $n-1$ : ce qui provoquera 1 opération.

$1+2+3+...+(n-2)+(n-1)=\dfrac{n \times (n-1)}{2}=\dfrac{1}{2}n^2 - \dfrac{1}{2}n$

Le terme de plus haut degré est $n^2$, c'est donc lui qui va déterminer la manière dont le nombre d'opérations évolue en fonction de $n$.

Cela signifie dans notre cas que le nombre d'opérations va évoluer avec le carré du nombre de termes de la liste à trier.

!!! warning ""

    La **complexité** du tri par sélection est dite ***quadratique***, que l'on note aussi 
    $\theta (n^2)$.

### 1.4 Terminaison de l'algorithme de tri par sélection

Comment prouver que notre algorithme va terminer quelle que soit le tableau fournit 
en entrée ?

Notre algorithme fait intervenir deux boucles "Pour" imbriquées. Ces boucles sont
bornées par la taille du tableau, notre tableau n'étant pas de taille infinie, il 
est évident que l'algorithme s'arrêtera au bout d'un nombre fixe d'opérations.

D'après les calculs précédent, ce nombre est de l'ordre de $\dfrac{n \times (n-1)}{2}$
opérations. 

Nous prouvons ainsi que notre algorithme terminera quel que soit le tableau (fini) fournit
en entrée.

### 1.5 Correction de l'algorithme de tri par sélection

Nous venons de prouver que notre algorithme termine, mais comment prouver qu'il
va bien réaliser à chaque fois ce qu'on attend de lui ?

Les preuves de correction sont des preuves théoriques. La preuve ici s'appuie sur le concept mathématique de **récurrence**. 

!!! tips "Principe du raisonnement par récurrence"

        une propriété $P(n)$ est vraie si :

        - $P(0)$ (par exemple) est vraie  
  
        - Pour tout entier naturel $n$, si $P(n)$ est vraie alors $P(n+1)$ est vraie.

!!! info "Preuve de la correction"

    Ici, la propriété serait : « Quand $k$ varie entre `0` et `longueur(liste) - 1`, la sous-liste de longueur $k$ est triée dans l'ordre croissant.» On appelle cette propriété un **invariant de boucle** (sous-entendu : elle est vraie pour chaque boucle).

    - quand $k$ vaut `0`, on place le minimum $m_0$ de la liste en position `0`, la sous-liste [$m_0$] est donc triée.
    -  si la sous-liste de $k$ éléments [$m_0, m_1, ..., m_{k-1}$] est triée, l'algorithme rajoute en dernière position de la liste le minimum de la sous-liste restante, dont tous les éléments sont supérieurs au maximum de la sous-liste de $k$ éléments. La sous-liste de $k+1$ éléments [$m_0, m_1, ..., m_{k-1}, m_k$] est donc elle aussi triée.
      
    Au dernier tour de boucle, $k$ vaut `longueur(liste) - 1`. À la fin de ce tour tous les 
    éléments jusqu’à la case `longueur(liste) - 1` incluse sont à leur place définitive. Toute 
    la liste est donc triée.

## 2. Tri par insertion

### 2.1 Principe du tri par insertion

![illustration tri insertion](https://glassus.github.io/premiere_nsi/T4_Algorithmique/4.3_Tri_par_insertion/data/insertion1.gif)

On parcourt tous les éléments, chacun est inséré à sa place dans les éléments
déjà triés qui le précèdent. Cela correspond à l'algorithme communément utilisé par
un joueur pour trier les cartes dans sa main.

### 2.2 Algorithme du tri par insertion

On traite successivement toutes les valeurs à trier, en commençant par celle en deuxième 
position.

Traitement : tant que la valeur à traiter est inférieure à celle située à sa gauche, on 
échange ces deux valeurs.

!!! warning "Algorithme"

    ```
    fonction tri_insertion(T : Tableau)
        Pour i de 1 à longueur(T) - 1, faire
                x ← T[i]                            
                j ← i                               
                Tant que j > 0 et T[j - 1] > x
                        T[j] ← T[j - 1]
                        j ← j - 1
                Fin Tant que
                T[j] ← x
        Fin Pour
    Fin fonction
    ```


!!! example "Implémentation en Python"

    L'implémentation est présentée dans l'IDE ci-dessous, vous pouvez la
    tester avec quelques tableaux.

    {{IDEv('res/tri_insertion')}}


### 2.3 Complexité de l'algorithme de tri par insertion

Dans le pire des cas, le tri par insertion n’est pas meilleur que le tri par
sélection. Il faut alors $1 + 2+ ... +(n—2)+(n—1)$ opérations pour trier un tableau 
de taille $n$.

!!! warning ""

    La **complexité** du tri par insertion est donc elle aussi 
    ***quadratique*** ($\theta (n^2)$).

En revanche, il existe des situations où le tri par insertion est meilleur 
que le tri par sélection. C'est  notamment le cas lorsque le tableau est
presque déjà trié.


### 2.4 Terminaison

Notre algorithme de tri par insertion fait intervenir une première
boucle `Pour` et l'arrêt de celle-ci est déterminé par la taille du tableau.

Celui-ci étant de taille fini, cette boucle terminera de façon certaine.

Cette première boucle contient une boucle `Tant que` dont le critère d'entrée 
est la conjonction de deux conditions dont l'une est `j>0`, or `j` est une valeur 
strictement positive qui est décrémentée à chaque tour de boucle. Il est donc
certain que la condition `j>0` devienne fausse et que donc la boucle se termine.

Ces deux boucles terminant, notre algorithme termine lui aussi quel que soit le 
tableau (fini) fournit en entrée.

### 2.5 Correction

Comme pour le tri par sélection, nous allons utiliser un invariant de boucle 
pour montrer que l'algorithme de tri par insertion est correct. 

!!! info "Preuve de la correction"

    Dans la boucle `Pour`nous allons utiliser l'invariant suivant : à chaque tour
    de boucle les éléments sont triés jusqu'à l'indice `i`.

    - Cette propriété est vraie en entrant dans la boucle car `i` vaut alors 1
    et la liste réduite à la case d'indice 0 est triée.  
    - Supposons que les éléments soient triés jusqu'à l'indice `i-1`, lors du tour
      `i`, l'élément `T[i]`sera inséré correctement de tel sorte que tous les éléments
      plus grands se situeront après lui.

    En conséquence, lorsque la boucle `for` termine son dernier tour, `i` désigne la
    dernière case et on a montré que la liste était triée jusqu’à cette case. La liste est
    donc entièrement triée.

## 3. Comparaison des algorithmes de tris

Dans l'animation suivante on retrouve les deux algorithmes étudiés, ainsi que 
d'autres qui seront vus en classe de Terminale.

On peut y constater que l'algorithme de tri par insertion est plus efficace 
que le tri par sélection lorsque les éléments sont quasiment triés.

![algo de tris](https://glassus.github.io/premiere_nsi/T4_Algorithmique/4.4_Tri_par_selection/data/comparaisons.gif)


Vous pouvez retrouver et comparer vous même ces algorithmes sur ce [site](https://www.toptal.com/developers/sorting-algorithms).

---

*références* :

 - T.Balabonski, S.Conchon, JC.Filliâtre, K.Nguyen  *Numérique et Sciences Informatiques*, Ed. Ellipses
 - C.Adobet, G.Connan, G.Rozsavolgyi, L.Signac *Numérique et sciences informatiques SPÉCIALITÉ*, Ed. Hatier
 - G.Lassus (lycée François Mauriac, Bordeaux), [Première NSI - Algorithmique - tris](https://glassus.github.io/premiere_nsi/T4_Algorithmique/4.3_Tri_par_insertion/cours/)