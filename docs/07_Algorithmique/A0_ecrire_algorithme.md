---
author: Bruno Bourgine
title: Écrire un algorithme
hide:
  - footer
---


## 1. Notion d'algorithme

!!! info "Une définition"

    On peut proposer la définition d'un algorithme comme étant une suite **d'opérations non-ambiguës** permettant de résoudre une **catégorie de problème** en un **nombre fini** d'étapes.

En effet :

- La description des opérations de l'algorithme doit donc être **explicite** pour ne pas
laisser la place à de fausses interprétations.  
- Un algorithme résoud une **catégorie de problèmes** et pas une *instance* (un cas unique)
de problème. Par exemple on est maintenant capable de ranger n'importe quel tas de crêpes
et non pas un tas en particulier.  
- Un algorithme doit **terminer** pour n'importe quel problème entrant dans la catégorie qu'il
traite. Ainsi, même si le tas comporte un million de crêpes, on pourra le ranger en un temps non-infini (mais très long quand même).  

Par ailleurs :  

- un algorithme peut recevoir des données en **entrées** sur lesquelles il
va travailler,  
- un algorithme renvoie généralement un résultat en **sortie**,  
- un algorithme doit utiliser des opérations basiques (telles qu'un être humain pourrait
 les réaliser).  

## 2. Langage

Si un algorithme se doit d'être explicite, encore faut-il utiliser un langage
qui permette d'exclure toute ambiguïté. On peut considérer un langage comme
une collection de symbole auxquels on donne sens.

### 2.1 Les diagrammes

Même s'ils ne sont à proprement parler un langage, les diagrammes permettent
d'exprimer un algorithme, on appelle parfois ceux-ci des *algorigrammes*. Ils
permettent la visualisation globale d'un algorithme, et facilite la compréhension
du traitement sur une entrée donnée.

!!! example "Algorithme de calcul du PGCD"

    L'algorithme de calcul du Plus Grand Commun Diviseur est attribué au mathématicien
    grec [Euclide](https://fr.wikipedia.org/wiki/Euclide).

    ![algorigramme euclide](res/algorigramme_euclide2.png)

      

### 2.2 Le pseudo-code

Le pseudo-code permet d'écrire un algorithme en langage naturel, tout en introduisant
un certain formalisme mais sans faire référence à un langage de programmation. Il 
n'existe pas de convention pour l'écriture en pseudo-code.

Nous utiliserons les conventions suivantes :

!!! info "Structure générale "

    Un algorithme est précédé de `Déclarations`, qui permet 
    de préciser les variables utilisées en donnant leur nom et leur
    type (entier, chaîne de caractère...)

    On écrit l'algorithme écrit entre les mot clés `Début` et `Fin`.

    Les instructions s'écrivent ligne après ligne et sont indentées
    (décalées horizontalement) si besoin.

    ```
    Déclarations :
        ...
    Début
        ...
    Fin
    ```

!!! info "Affectation"

    L'affectation est l'action d'attribuer une valeur à une variable, 
    on la représente par ←.



!!! info "Instruction conditionnelle"

    Pour indiquer une instruction conditionnelle, on utilise la structure :

    ```
    Si (condition) Alors
        (instruction)
        ...
    FinSi
    ```

    ou selon le besoin :

    ```
    Si (condition)  Alors
        (instruction)
        ...
    Sinon 
        (instruction)
        ...
    FinSi
    ```

!!! info "Itérations"

    Une itération désigne l'action de répéter un processus, un bloc d'instruction.
    On utilisera deux façon d'itérer : avec une boucle `Pour` et avec une boucle
    `Tant Que`.

    La boucle `Pour` est dite *bornée* car le nombre d'itérations y est défini avant
    de commencer.

    ```
    Pour ... de ... à ... Faire
        (instruction)
        ...
    ``` 

    Le principe de la boucle `Tant que` est que l'on ne connait pas à l'avance le 
    nombre d'itérations, on dit qu'elle est *non-bornée*.

    ```
    Tant Que (condition) Faire
        (instruction)
        ...
    Fin TantQue

    ```

!!! info "Lecture/Écriture"

    Pour signifier l'interaction avec un utilisateur, il faut parfois introduire
    des actions de lecture par l'algorithme d'une valeur donné par l'utilisateur.

    `entree ← Lire()`

    Inversement on doit pouvoir indiquer que l'algorithme doit écrire un résultat
    afin que l'utilisateur en prenne connaissance.

    `Écrire("texte à afficher")`


  


!!! example "Exemple d'algorithme en pseudo-code"

    Algorithme du calcul du PGCD  


    ```
    Déclarations :
        a, b : entiers positifs
    Début
        r ← reste de la division euclidienne de a par b
        Tant Que r ≠ 0 Faire
            a ← b
            b ← r
            r ← reste de la division euclidienne de a par b
        FinTantQue
        Écrire(r)
    Fin
    ```


### 2.3 Les langages de programmation

Un langage de programmation permet de traduire un algorithme de façon à ce qu'il
puisse être exécuté par une machine. Le programme qui nous servira de support sera
principalement Python, mais il en existe des centaines d'autres.

!!! info "Intérêt du pseudo-code"

    Le soin apporté à l'écriture d'un algorithme en pseudo-code facilitera d'autant plus sa traduction en un langage de programmation.

