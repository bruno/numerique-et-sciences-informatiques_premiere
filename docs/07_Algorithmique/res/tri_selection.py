def tri_selection(tab : list) -> None :
    taille = len(tab)
    for i in range(taille-1):
        indice_min = i
        for j in range(i+1,taille):
            if tab[j] < tab[indice_min] :
                indice_min = j
        tab[i],tab[indice_min] = tab[indice_min], tab[i]
