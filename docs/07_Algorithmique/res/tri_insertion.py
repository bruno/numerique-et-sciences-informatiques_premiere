def tri_insertion(tab : list) -> None :
    "trie en place le tableau donné en paramètre"
    for i in range(len(tab)):
        k = i
        while tab[k] < tab[k-1] and k > 0 :
            tab[k], tab[k-1] = tab[k-1], tab[k]
            k = k - 1
