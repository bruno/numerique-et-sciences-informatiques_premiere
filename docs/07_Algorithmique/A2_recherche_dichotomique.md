---
author: Bruno Bourgine
title: Recherche dichotomique dans un tableau trié
hide:
  - footer
---


##  1. Introduction : recherche d'une valeur dans une liste

### 1.1 Préambule

!!! note "Pourquoi trie-t-on ?"
    
    Quel est l'intérêt de trier ses données ?
    
    - l'intérêt immédiat est d'en tirer un classement : quelle est la plus grande (ou plus petite) valeur, la deuxième, la troisième... On s'en sert donc évidemment pour déterminer une valeur optimale, un gagnant dans une compétition, etc. Mais il y a une autre raison plus importante.

    - **Trier ses données permet de rechercher plus rapidement une valeur précise parmi celles-ci.**



**Exemple :** pouvez-vous deviner la couleur à laquelle je pense ?


```python
coul = ["bleu", "jaune", "rouge", "vert", "violet", "marron"]
```

Toutes les méthodes (proposition des valeurs dans l'ordre, au hasard, dans 
l'ordre inverse...) sont équivalentes : elles sont toutes aussi mauvaises, 
aucune stratégie n'est possible car les données ne sont pas triées. Si je suis 
à la recherche de la valeur "vert", le fait de piocher "rouge" ne me donnera 
aucune indication sur le fait que je devrais chercher plus à gauche à plus à 
droite que l'endroit où j'ai pioché.

Il faudrait pour cela que la liste soit triée (et donc qu'elle soit «triable», 
ce qui n'est pas toujours le cas !). C'est donc le cas dans lequel nous allons 
nous placer dans toute la suite de ce cours. 

### 1.2 Contexte de recherche

Considérons donc la liste ```lst```  suivante : 

![image](res/fig0.png){: .center}

```python
lst = [2, 3, 6, 7, 11, 14, 18, 19, 24]
```

L'objectif est de définir un algorithme de recherche efficace d'une valeur arbitraire présente dans cette liste.

### 1.3 Méthode naïve : recherche séquentielle

C'est la méthode la plus intuitive : on essaie toutes les valeurs (par exemple, dans l'ordre croissant) jusqu'à trouver la bonne.


!!! question "Recherche de l'indice d'un élément"

    Écrire une fonction `recherche_naive` qui reçoit pour paramètres une liste 
    `lst` d'entiers et un nombre entier `val` et qui renvoie l'indice de `val`
    dans la liste `lst `.
     
    Si la valeur `val` n'est pas trouvée, on renverra -1.

    {{ IDEv() }}

    ??? success "Correction"

    ```python
    def recherche_naive(lst : list, val, int) :
      n = len(lst)
      for i in range(n) :
        if val == lst[i] :
          return i
      return -1
    ```


### 1.4 Complexité de la méthode naïve

!!! note "Complexité de la méthode naïve :heart:"

    Dans le cas d'une recherche naïve, le nombre (maximal) d'opérations 
    nécessaires est proportionnel à la taille de la liste à étudier. Si on 
    appelle $n$ la longueur de la liste, on dit que cet algorithme est 
    **d'ordre $n$**, ou **linéaire**, ou en $O(n)$.

**Remarque :** 
La méthode naïve n'utilise pas le fait que la liste est triée, on aurait pu 
aussi bien l'utiliser sur une liste non triée.



## 2. Recherche dichotomique

### 2.1 Introduction : le jeu du *«devine un nombre entre 1 et 100»*

!!! abstract "Règles du jeu"

    Si je choisis un nombre entre 1 et 100, quelle est la stratégie optimale 
    pour deviner ce nombre le plus vite possible ?  
    (à chaque étape, une indication (trop grand, trop petit) permet d'affiner 
    la proposition suivante)

**Réponse attendue :** la meilleure stratégie est de *couper en deux* à chaque fois l'intervalle d'étude. On démarre de 50, puis 75 ou 25, etc.

#### Conclusion générale de l'activité d'introduction

La stratégie optimale est de diviser en deux à chaque étape l'intervalle 
d'étude. On appelle cela une méthode par **dichotomie**, du grec ancien 
διχοτομία, dikhotomia (« division en deux parties »).

### 2.2 Algorithme de recherche dichotomique

!!! note "Spécification"

    On cherche à écrire une fonction `recherche_dichotomique(v, tab)` qui 
    recherche une valeur `v` dans un tableau trié `tab` et en renvoie son 
    indice `i` . Si `v` est absent la fonction renvoie -1.
    En voici sa spécification :

    - **Entrée(s)** : un tableau `tab` de taille `n` constitué d'entiers, un 
     entier `v`.
    - **Sortie(s)** : un entier.
    - **Rôle** : renvoyer l'indice de `v` si `v`∈`tab` , -1 sinon.
    - **Précondition(s)** : `tab` est trié par ordre croissant.
    - **Postcondition(s)** : si `v` est dans `tab`, `i` correspond à son indice, 
     sinon `i` = -1.

!!! warning "Principe de l'algorithme de recherche dichotomique :"

    Trouver la position la plus centrale du tableau (si le tableau est vide, sortir 
    et renvoyer -1).

    Comparer la valeur de cette case à l'élément recherché `v`. Trois cas se présentent :

    - Si `v` est égale à la valeur centrale, alors renvoyer la position.
    - Si `v` est strictement inférieure à la valeur centrale, alors reprendre la 
         procédure dans la moitié gauche du tableau.
    - Si `v` est strictement supérieure à la valeur centrale, alors reprendre la 
         procédure dans la moitié droite du tableau.

**Illustration**

![](https://upload.wikimedia.org/wikipedia/commons/f/f7/Binary_search_into_array.png)
*source : Wikipedia*


### 2.3 Programmation de la méthode de dichotomie

Pour des raisons d'efficacité, nous allons garder *intacte* notre liste de travail et 
simplement faire évoluer les indices qui déterminent le début et la fin de notre liste.

Nous allons donc travailler avec trois variables :

- `indice_debut` (en bleu sur le schéma)
- `indice_fin` (en bleu sur le schéma)
- `indice_central`, qui est égale à `(indice_debut + indice_fin) // 2` (en rouge sur le schéma)
  
![image](res/fig4.png){: .center}

Nous allons faire *se rapprocher* les indices `indice_debut` et `indice_fin` **tant que** `indice_debut <= indice_fin`


!!! warning "Recherche dichotomique dans une liste triée"
    
    ```python
    def recherche_dichotomique(v, tab) :
        indice_debut = 0
        indice_fin = len(tab) - 1
        while indice_debut <= indice_fin :
            indice_centre = (indice_debut + indice_fin) // 2     
            valeur_centrale = tab[indice_centre]            
            if valeur_centrale == v :          
                return indice_centre
            if valeur_centrale < v :             
                indice_debut = indice_centre + 1
            else :
                indice_fin = indice_centre - 1
        return -1
            
    ```
    

**Utilisation**

```python
>>> mon_tableau = [2, 3, 6, 7, 11, 14, 18, 19, 24]
>>> recherche_dichotomique(mon_tableau, 14)
5
>>> recherche_dichotomique(mon_tableau, 2)
0
>>> recherche_dichotomique(mon_tableau, 24)
8
>>> recherche_dichotomique(mon_tableau, 1789)
>>> 
```

### 2.4 Terminaison de l'algorithme

Est-on sûr que l'algorithme va se terminer ?  
La boucle `while` qui est utilisée doit nous inciter à la prudence. 
Il y a en effet le risque de rentrer dans une boucle infinie.  
Pourquoi n'est-ce pas le cas ?

**Aide :** observer la position des deux flèches bleues lors de l'exécution de l'algorithme 
![image](res/fig4.png){: .center}



La condition de la boucle `while` est `indice_debut <= indice_fin `, qui pourrait aussi s'écrire `indice_fin >= indice_debut `.  
Au démarrage de la boucle, on a :


```python
    indice_debut = 0
    indice_fin = len(L) - 1
```

Ceci qui nous assure donc de bien rentrer dans la boucle. 

Ensuite, à chaque étape :

- soit `indice_debut` augmente strictement (grâce à ```#!python indice_debut = indice_centre + 1```)
- soit  `indice_fin` diminue strictement (grâce à ```#!python indice_fin = indice_centre - 1```)

Il va donc forcément arriver un moment où `indice_fin` sera inférieur à `indice_debut` : on sortira alors de la boucle et le programme va bien se terminer.

**Variant de boucle**  
On dit que la valeur `indice_fin - indice_debut ` représente le **variant de boucle** de 
cet algorithme. 
Ce variant est un nombre entier, d'abord strictement positif, puis qui va décroître 
jusqu'à la valeur 0.

### 2.6 Complexité de l'algorithme

Combien d'étapes (au maximum) sont-elles nécessaires pour arriver à la fin de 
l'algorithme ?  

Imaginons que la liste initiale possède 8 valeurs.

 - Après une étape, il ne reste que 4 valeurs à traiter.  
 - Puis 2 valeurs.   
 - Puis une seule valeur.  

Il y a donc 3 étapes avant de trouver la valeur cherchée.

!!! question "Exercices"
    
    Q1. Remplissez le tableau ci-dessous :

    | taille de la liste | 1 | 2 | 4 | 8 | 16 | 32 | 64 | 128 | 256 |
    | :----------------- |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    | nombre d'étapes    | _ | _ |  _  |   3 |  _ | _   | _   | _   | _   |  _  |

    Q2. Pouvez-vous deviner le nombre d'étapes nécessaires pour une liste de 4096 termes ?

    Q3. Pour une liste de $2^n$ termes, quel est le nombre d'étapes ?
    
   
    ??? success "Correction" 
         Q1. 

        | taille de la liste | 1 | 2 | 4 | 8 | 16 | 32 | 64 | 128 | 256 |
        | :----------------- |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
        | nombre d'étapes    | 0 |1 |  2  |   3 |  4 | 5   | 6   | 7   | 8   |  9  |

        Q2. $2^{12}=4096$ donc il faut 12 étapes.

        Q3. Il faut $n$ étapes.
        """
   

**Conclusion :** 

C'est le nombre de puissances de 2 que contient le nombre $N$ de termes de la liste qui est déterminant dans la complexité de l'algorithme. 

Ce nombre s'appelle le *logarithme de base 2* et se note $\log_2(N)$.

 On dit que l'algorithme de dichotomie a une **vitesse logarithmique**. On rencontrera parfois la notation $O(\log_2(n))$.

!!! note "Complexité de la dichotomie :heart: :heart: :heart:"
    La recherche dichotomique se fait avec une **complexité logarithmique**.


Cette complexité est bien meilleure qu'une complexité linéaire. Le nombre 
d'opérations à effectuer est très peu sensible à la taille des données d'entrée, 
ce qui en fait un algorithme très efficace.

Par exemple, si on faisait une recherche dichotomomique sur les 8 milliards 
d'êtres humains de la planète (en admettant qu'on ait réussi à les classer...),
 il suffirait de 33 étapes pour trouver l'individu cherché !

(car $2^{33}= 8589934592$)


**Remarque :** Il ne faut toutefois pas oublier que la méthode dichotomique, bien plus 
rapide, nécessite que la liste ait été auparavant triée. Ce qui rajoute du temps de 
calcul ! 


!!! abstract "sources"

  - [G.LASSUS, Lycée François Mauriac - Bordeaux](https://glassus.github.io/premiere_nsi/T4_Algorithmique/4.5_Dichotomie/cours/) 
  - [Germain BECKER & Sébastien POINT, Lycée Mounier, ANGERS ](https://info-mounier.fr/premiere_nsi/algorithmique/recherche-dichotomique) 