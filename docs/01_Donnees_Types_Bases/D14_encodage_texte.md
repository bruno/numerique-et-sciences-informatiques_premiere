---
author : Bruno Bourgine, François Decq, Pascal Padilla
title : Encodage du texte
hide : 
 - footer
---



## 1. Représentation des caractères

Des normes définissent comment représenter les caractères. Selon les caractères à représenter, on peut utiliser :

- Unicode
- ASCII
- UTF-8
- etc.


!!! note "Pourquoi toutes ces normes ?"
    Historiquement, la représentation des lettres a été initiée par une équipe anglo-saxone. Ainsi dans cette première norme, seuls les caractères non accentués de l'alphabet latin étaient représentés. 

    La norme a naturellement évoluée pour y ajouter de nouveaux caractères, de nouvelles langues, de nouveaux symboles...

!!! warning "Règle importante"
    Les normes d'encodage de caractère respectent la **casse**. C'est-à-dire que les lettre minuscules et majuscules sont différenciées (*a* $\neq$ *A*).



## 2. ASCII (American Standard Code for Information Interchange)

### 2.1 ASCII basique (1960..1970)

Norme américaine d'encodage des caractères :

- Ne sait représenter que les caractères américains. Par exemple le caractère *é* n'est pas représenté.
- Unicité des correspondances : un code $\iff$ un caractère.
- Codé sur 7 bits avec donc 128 caractères représentés.

![](./source/ASCII-Table.png)


### 2.2 ASCII étendu (1970..1990)

Avec l'arrivée de l'informatique en Europe, il a fallu étendre la norme ASCII... tout en conservant la compatibilité avec la norme précédente.

- Ajout d'un $8^{\text{ème}}$ bit. Il y a donc 2 fois plus de caractères représentés.
- Pour l'europe, cette norme est appelée *ISO/CEI 8859-1*, ou encore *Latin-1*, ou encore *Europe Occidentale*.
- Il existe de nombreuses variations de la norme ASCII étendue.

!!! example "Exemple - table ASCII étendue Latin-1"
    ![](./source/ASCII-Table-latin-1.png)


## 3. Unicode (1990..)

Le multitude de tablea à travers le monde pose problème.

Pour unifier au niveau mondial, il a été écrit (enfin)  une norme commune : **Unicode** ([source](https://www.unicode.org/charts/fr/)) aujourd'hui utilisée pour les claviers d'ordinateurs.

!!! note Principes et remarques

    - **Point de code** : identifiant unique pour chaque caractère existant.
    - Les 128 premiers points de code sont compatibles avec ASCII
    - Il existe $1~114~112$ points de code, représentés sur l'intervalle $[0000~0000_{\small 16} .. 0010~FFFF_{\small 16}] (2^{20} = 1~048~576)$.
    - Les 65~536 premiers points de code (2 octets) représentent les caractères les plus communément utilisés dans le monde, appelés les BMP (Basic Multilingual Plane).

!!! example "Exemple"
    '&#x00000041;' a pour point de code $0000~0041_{\small 16}$.

!!! note "Extrait Unicode - ASCII"

    === "ASCII"

        ![](./source/unicode_ASCII.png)

    === "ASCII Étendu - Latin-1"

        ![](./source/unicode_ASCII_etendu.png)

    === "ASCII Étendu - Grec et Copte"

        ![](./source/unicode_grec_copte.png)

    === "ASCII Étendu - Chinois"

        ![](./source/unicode_CJC.png)

    === "ASCII Étendu - Ougarique"

        ![](./source/unicode_ougarique.png)


### 3.1 UTF-32 (Unicode Transformation Format sur 32 bits)


- Codage sur 32 bits / 4 octets.
- Un point de code $\iff$ une représentation UTF-32
- Actuellement, 1 octet inutilisé (!)
- Utile si on souhaite une longueur fixe ou un accès unique des caractères


!!! example "Exemples Unicode"

    === "ASCII"

        '&#x0041;' (point de code $0041_{\small 16}$) a pour code $\mathtt{0000~0041_{\small 16}}$

    === "ASCII étendu Grec et Copte"

        '&#x03a9;' (point de code $03A9_{\small 16}$) a pour code $\mathtt{0000~03A9_{\small 16}}$

    === "Chinois"

        '&#x6587;' (point de code $6587_{\small 16}$) a pour code $\mathtt{0000~6587_{\small 16}}$

    === "Ougarique"

        '&#x00010384;' (point de code $10384_{\small 16}$) a pour code $\mathtt{0001~0384_{\small 16}}$



### 3.2 UTF-16 (Unicode Transformation Format sur 16 bits)

- Codage sur 16 bits / 2 octets.
- Représentation des points de code de l'intervalle $[0000_{\small 16} \ldots FFFF_{\small 16}]$
- Les caractères de l'intervalle $[01~0000_{\small 16}\ldots 10FFFF_{\small 16}]$ sont représentés par des paires de code de 16bits.
- Codage de longueur variable : 1 ou 2 codes.
- Optimisé pour BMP ($65~536$ premiers points de code).



!!! example "Exemples Unicode"

    === "ASCII"

        '&#x0041;' (point de code $0041_{\small 16}$) a pour code $\mathtt{0041_{\small 16}}$

    === "ASCII étendu Grec et Copte"

        '&#x03a9;' (point de code $03A9_{\small 16}$) a pour code $\mathtt{03A9_{\small 16}}$

    === "Chinois"

        '&#x6587;' (point de code $6587_{\small 16}$) a pour code $\mathtt{6587_{\small 16}}$

    === "Ougarique"

        '&#x00010384;' (point de code $10384_{\small 16}$) a pour code $\mathtt{D800~DF84_{\small 16}}$

!!! warning "Attention"
    Vocabulaire : **code** $\neq$ **point de code** !!!



### 3.3 UTF-8 (Unicode Transformation Format sur 8 bits)

- Codage sur 8 bits / 1 octet
- Représentation des points de code de l'intervalle $[00_{\small 16} \ldots FF_{\small 16}]$.
- Les caractères de l'intervalle $[00~0100_{\small 16}\ldots 10FFFF_{\small 16}]$ sont représentés par 2 à 4 codes de 8 bits.
- Codage de longueur variable : 1 à 4 codes.
- Optimisé pour ASCII et la majorité des langages.
- Est représenté sur 1 octet (unité la plus utilisée !).


!!! example "Exemples Unicode"

    === "ASCII"

        '&#x0041;' (point de code $0041_{\small 16}$) a pour code $\mathtt{41_{\small 16}}$

    === "ASCII étendu Grec et Copte"

        '&#x03a9;' (point de code $03A9_{\small 16}$) a pour code $\mathtt{CE~A9_{\small 16}}$

    === "Chinois"

        '&#x6587;' (point de code $6587_{\small 16}$) a pour code $\mathtt{E6~96~87_{\small 16}}$

    === "Ougarique"

        '&#x00010384;' (point de code $10384_{\small 16}$) a pour code $\mathtt{F0~90~8E~84_{\small 16}}$


### 3.4 Méthode de représentation pour UTF-8 et UTF-16

Comment savoir le nombre de points de code à utiliser pour représenter un caractère ?

Le codage des caractères utilise des **préfixes** lorsque plusieurs points de code

!!! warning "Méthode d'encodage UTF-16 et UTF-8"

    === "UTF-16"

        $$
        \begin{array}{|c|r|r|}\hline
        \textbf{Intervalle} \text{ (hexadécimal)}& \textbf{Point de code} \text{ (binaire)} & \textbf{Code(s) UTF-16} \text{ (binaire)} \\ \hline
        [0000 \ldots FFFF] & xxxx~xxxx~xxxx~xxxx & \overbrace{xxxx~xxxx~xxxx~xxxx}^{\text{code UTF-16}} \\ \hline
        [01~0000\ldots 10FFFF] & 000y~yyyy~xxxx~xxxx~xxxx~xxxx & \overbrace{1101~10zz~zzxx~xxxx}^{\text{code UTF-16}}~\overbrace{1101~11xx~xxxx~xxxx}^{\text{code UTF-16}} \\
        & & \text{avec $z$ tel que $zzzz = yyyyy - 1$}\\\hline 
        \end{array}
        $$

        !!! note "Remarque"

            Soit un caractère nécessitant 2 codes pour un encodage UTF-16 :
            
            - Le plus petit code possible est atteint par `1101 1000 0000 0000` (qui correspond à `D800`).
            - Le plus grand code possible est atteint par `1101 1111 1111 1111` (qui correspond à `DFFF`).
            
            Donc dans cas de 2 codes nécessaires, les codes appartiennent à l'intervalle `[D800...DFFF]`.



    === "UTF-8"

        $$
        \begin{array}{|c|r|r|}\hline
        \textbf{Intervalle} \text{ (hexadécimal)}& \textbf{Point de code} \text{ (binaire)} & \textbf{Code(s) UTF-16} \text{ (binaire)} 
        \\ \hline
        [00 \ldots 7F] 
        & 0xxx~xxxx 
        & \overbrace{0xxx~xxxx}^{\text{code UTF-8}} 
        \\ \hline
        [80\ldots 07FF] 
        & 0000~0yyy~yyxx~xxxx 
        & \overbrace{110y~yyyy}^{\text{code UTF-8}}~\overbrace{10xx~xxxx}^{\text{code UTF-8}}
        \\\hline 
        [0800\ldots FFFF] 
        & zzzz~yyyy~yyxx~xxxx 
        & \overbrace{1110~zzzz}^{\text{code UTF-8}}~\overbrace{10yy~yyyy}^{\text{code UTF-8}}~\overbrace{10xx~xxxx}^{\text{code UTF-8}}
        \\\hline 
        [01~0000\ldots 10~FFFF] 
        & 000u~uuuu~zzzz~yyyy~yyxx~xxxx 
        & \overbrace{1111~0uuu}^{\text{code UTF-8}}~\overbrace{10uu~zzzz}^{\text{code UTF-8}}~\overbrace{10yy~yyyy}^{\text{code UTF-8}}~\overbrace{10xx~xxxx}^{\text{code UTF-8}}
        \\\hline 
        \end{array}
        $$

## 4. Autres représentations

Il existe bien sûr d'autres représentations :

- Windows 1252 (Microsoft en Europe)
- EBCDIC (IBM pour cartes perforées)
- ISCII (Inde)
- VISCII (Vietnam)
- etc.

## 5. Et après...

Mais alors, lorsque la machine reçoit 32 bits de données : comment sait-on si c'est un entier naturel, un entier relatif, un réel, une suite de booléens ou un texte ?

Comment savoir quelle valeur est représentée ?

C'est le principe du **typage de données** !
