---
author: Bruno Bourgine, François Decq, Pascal Padilla
title: Représentation des entiers relatifs et des réels
hide: 
  - footer
---

## 1. Représentation d'un entier relatif en binaire

Dans cette partie, nous détaillerons comment encoder un entier relatif appartenant à $\mathbb{Z}$.

### 1.1 Complément à deux

!!! info "Complément à deux"
    Voici la méthode pour écrire le complément à deux d'un nombre entier relatif:
    
    1. Inverser les bits
    2. Ajouter $1$ au résultat

!!! example "Exemple"
    Complément à deux de $1011~0001$ :

    1. Inverser les bits : $1011~0100_2 \rightarrow 0100~1011_2$
    2. Ajouter 1 : $0100~1011_2 + 1 = 0100~1100_2$

### 1.2 Représentation d'un entier relatif

!!! warning "Méthode pour encoder un entier relatif"
    * Définir la longueur de la représentation en binaire
    * Bit de poids fort (à l'extrême gauche) 
        * $=0$ si le nombre est positif ou nul
        * $=1$ sinon 
    * Le reste des bits représente l'entier.
    * Pour un entier positif : codage des entiers naturels.
    * Pour un entier négatif : principe du **complément à deux** sur l'écriture binaire de la valeur absolue.

!!! example "Exemple"
    Représenter $-65$ sur 8 bits. Comme $-65_10$ est négatif, il faut appliquer le complément à deux sur $|-65_{\small 10}|$ représenté en binaire:

    1. Représentation binaire : $|-65_{\small 10}| = 65_{\small 10} = 0100~0001_2$
    2. Complément à deux 1/2 : $0100~0001_2 \rightarrow 1011~1110_2$
    3. Complément à deux 2/2 : $1011~1110_2 + 1 = 1011~1111_2$.
    4. Représentation : $-65_{\small 10} = 1011~1111_2$

## 2. Représentation d'un nombre réel

### 2.1 Généralités

Pour représenter les nombres décimaux, on décompose encore les nombres en puissances de 10 (en décimal) et de 2 (en binaire) mais cette fois-ci, avec des exposants qui peuvent être **négatifs**.

!!! example "Exemples"
    * Somme de puissance de 10 : 
    $652,375 = 6\times10^{2} + 5\times10^{1} + 2\times10^{0} + 3\times10^{-1} + 7\times10^{-2} + 5\times10^{-3}$
	* Somme de puissances de 2 :
    $110,101_2 = 1\times2^{2} + 1\times2^{1} + 0\times2^{0} + 1\times2^{-1} + 0\times2^{-2} + 1\times2^{-3}$

### 2.2 Représentation en virgule fixe

#### Binaire vers décimal

$$
\begin{array}
110,101_2 &=& 1\times2^{2} &+& 1\times2^{1} &+& 0\times2^{0} &+& 1\times2^{-1} &+& 0\times2^{-2} &+& 1\times2^{-3} \\
&=& 4 &+& 2 &+& 0 &+& \underbrace{1/2}_{0,5} &+& 0 &+& \underbrace{1/8}_{0,125} \\
&=& 6,625
\end{array}
$$

#### Décimal vers binaire



!!! warning "Méthode pour déterminer la représentation de la partie décimale"
    Pour déterminer les bits de la partie décimale, il faut:

    1. multiplier par 2 la partie décimale
    2. le bit courant vaut $1$ si le résultat est supérieur ou égale à 1 et $0$ sinon
    3. recommencer l'étape (1) avec la partie décimale du résultat.

!!! example "Exemple 1 - représenter $65,375_{\small 10}$"
    La partie entière vaut $65_{\small 10} = 100~0001_2$.
    
    La partie décimale vaut $0,375$ :

    $$
    \left.
    \begin{align*}
    0,375 \times 2 &=  \textbf 0,75 \\
    0,75 \times 2 &=  \textbf 1,5 \\
    0,5 \times 2 &=  \textbf 1,0 \\
    0 \times 2 &=  \textbf 0,0 \\
    0 \times 2 &=  \textbf 0,0 \\
    0 \times 2 &=  \textbf 0,0 \\
    \ldots
    \end{align*} \right\} 0,011000\ldots = 0,011
    $$

    Donc $65,375_{\small 10} = 100~0001,011_2$.
    

!!! example "Exemple 2 - représenter $2,6_{\small 10}$"
    La partie entière vaut $2_{\small 10} = 10_2$.

    La partie décimale vaut $0,6$:

    $$
    \left.\begin{align*}
    0,6 \times 2 &= \textbf 1,2 \\
    0,2 \times 2 &= \textbf 0,4 \\
    0,4 \times 2 &= \textbf 0,8 \\
    0,8 \times 2 &= \textbf 1,6 \\
    0,6 \times 2 &= \textbf 1,2 \\
    \ldots \\
    0,6 \times 2 &= \textbf 1,2 \\
    0,2 \times 2 &= \textbf 0,4 \\
    0,4 \times 2 &= \textbf 0,8 \\
    0,8 \times 2 &= \textbf 1,6 \\
    0,6 \times 2 &= \textbf 1,2 \\
    \ldots
    \end{align*}\right\} 0,1001~1001~1001\ldots
    $$

    Donc $2,6_{\small 10} = 10,1001~1001~1001\ldots_2$.


!!! info "Remarque"
    Deux cas se présentent :

    * Si la partie décimale est une somme de puissances de deux : suite de bits après la virgule **finie**.
    * Sinon : suite de bits après la virgule **infinie**.

    Ainsi, un nombre à développement décimal fini en base 10 ne l’est pas forcément en base 2 (en revanche, inversement : un nombre à développement décimal fini en base 2 l’est forcément en base 10).

    De plus, on ne parle pas ici des nombres à développement décimal infini en base 10...

### 2.3 Représentation en virgule flottante

Un ordinateur ne peut pas représenter n’importe quel nombre réel. Ce qui importe, c’est la **précision** avec laquelle on souhaite représenter le nombre réel.

Pour la représentation machine des nombres réels, un norme a été mise en place : *la norme IEEE 754*.
Elle définit la façon de coder un nombre réel sur 32 bits ou 64 bits.

!!! info "Définitions - Norme IEEE 754 sur 32/64 bits"
    La norme définit :

    - Le **signe** qui est représenté par le bit de poids fort.
    - L'**exposant** qui est un entier positif codé sur les 8/11 bits suivant le bit de signe.
    - La **mantisse** qui code sur les 23/52 bits restants la partie décimale du nombre.

    Sur 32 bits : ![](./source/IEEE75432.png)

    Sur 64 bits : ![](./source/IEEE75464.png)


!!! warning "Méthode de codage IEEE 754"
    
    1. Représenter le nombre en **virgule fixe**.
    2. Mise en **écriture scientifique** sous la forme : $1,\ldots \times 2^n$
    3. Codage de la **mantisse**.
    4. Détermination de l'**exposant** par décalage de $2^{\text{nb bits exposant}-1} - 1$.<br>
    Sur 32 bits, le décalage est de $2^{8-1}-1 = 127$ et pour 64 bits, il est de $2^{11-1}-1 = 1023$.
    5. Détermination du signe sur le bit de poids fort : $0$ pour positif et $1$ pour négatif.


!!! example "Exemple sur 32 bits"
    Représentons $65,375_{\small 10}$ selon la norme IEEE 754 sur 32 bits.

    1. $65,375_{\small 10} = 100~0001,011_2$
    2. $100~0001,011 = 1,0000~0101~1 \times 2^6$

        Pour la suite, nous avons donc : $\underbrace{+}_{\text{signe}}1,\underbrace{0000~0101~1}_{\text{mantisse}} \times 2^{\overbrace{6}^{\text{exposant}}}$

    3. Pour obtenir la mantisse, il suffit d'écrire la partie décimale sur 23 bits : $000~0010~1100~0000~0000~0000$.
    4. Sur 32 bits, le décalage est de 127. L'exposant sur 8 bits vaut donc $6+127 = 133_{\small 10} = 100~0010~1_{\small 2}$.
    5. Enfin, le bit de poids fort vaut $0$ car le nombre est positif.

    On obtient donc sur 32 bits : 
    
    $$
    \underbrace{0}_{\text{signe}}\underbrace{100~0010~1}_{\text{exposant}}\underbrace{000~0010~1100~0000~0000~0000}_{\text{mantisse}}
    $$

!!! info "Remarque - Pourquoi le décalage de l'exposant ?"

    Il est possible que l'exposant soit négatif (par exemple pour représenter des nombres tels que $0,0015542_{\small 10}$). Il faut donc pouvoir représenter un exposant négatif. <br>
    Ainsi il faut arriver à représenter les exposants compris entre $[-127...128]$ (sur 8 bits) ou $[-1023..1024]$ (sur 11 bits).
    
    Afin de ne manipuler que des entiers naturels ($\geq 0$) pour la représentation des exposants qui sont des entiers relatifs, la norme a décidé de décaler l'exposant de $127$ ou de $1023$.

!!! info "Remarque - Valeurs interdites"

    - L'exposant $0000~0000$ est interdit.
    - L'exposant $1111~1111$ est réservé pour signaler des erreurs (et est appelé NaN, Not a Number).

