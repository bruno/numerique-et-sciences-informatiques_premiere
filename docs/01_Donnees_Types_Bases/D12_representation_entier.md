---
author: Bruno Bourgine, François Decq, Pascal Padilla
title: Écriture d’un entier positif dans une base b⩾2
hide:
  - footer
---


Dans cette partie, nous détaillerons comment encoder un entier naturel (appartenant à l'ensemble $\mathbb{N}$).

## 1. Le décimal (Base 10)

!!! info "Vocabulaire"
    * *"déci"* signifie qu'il y a 10 termes possibles. Pour l'écriture on a choisi de $0$ à $9$.
    *  Le mot *chiffre* est un terme du langage courant pour désigner ces termes.
    * La base 10 est la base utilisée le plus couramment par les humains.



Dans ce cours, si $X$ est un nombre décimal, on le notera $X_{10}$ ou $X$.

## 2. Le binaire (Base 2)

!!! info Vocabulaire
    * *"bi"* signifie qu'il y a 2 termes possibles : $0$ et $1$.
    * Un *bit* (de l'anglais *binary digit*) est un terme du langage.

Dans ce cours, si $X$ est un nombre binaire, on le notera : $X_2$.

!!! example "Exemple"
    Exemple de notation en décimal et en binaire d'un même nombre :
    $1110~0101_2 = 229_{10}$

## 3. L'hexadécimal (Base 16)

!!! note Vocabulaire
    * *"hexadéci-"* signifie qu'il y a 16 termes possibles : de $0$ à $9$ et de $A$ à $F$.
    * Un terme du langage peut être traduit facilement par une suite de 4 bits.

Si $X$ est un nombre héxadécimal, on le notera $X_{16}$.

!!! example "Exemple"
    $E5_{16} = 1110~0101_2 = 229_{10}$

![Tableau d'équivalence entre terme hexadécimal et nombre décimal](./source/tab_hexa.png)

## 4. Le n-aire (Base n)

### Représentation générale en base n

Pour passer de la base décimale à une base n, il existe 2 méthodes :
* Décomposer le nmbre décimal en sommes de puissances de n.
* Remonter les restes des divisions euclidiennes successives du nombre par n.

!!! example "Exemple 1"
    $$
    \begin{align*}
      229_{10} &= \underbrace{1\times2^{7}}_{1} + \underbrace{1\times2^{6}}_{1} + \underbrace{1\times2^{5}}_1 + \underbrace{0\times2^{4}}_0 + \underbrace{0\times2^{3}}_0 + \underbrace{1\times2^{2}}_1 + \underbrace{0\times2^{1}}_0 + \underbrace{1\times2^{0}}_1\\
      &= 1110~0101_2
    \end{align*}
    $$

    ![](./source/exemple_1.png#center)

!!! example "Exemple 2"
    $$
    \begin{align*}
      229_{10} &= \underbrace{\underbrace{14\times 16^{1}}_{14}}_E + \underbrace{5\times 16^{0}}_{5} \\
      &= E5_{16}
    \end{align*}
    $$
    
    ![](./source/exemple_2.png)

!!! example "Exemple 3"
    $$
    \begin{align*}
      389_{10} &= \underbrace{\underbrace{19\times 20^{1}}_{19}}_J + \underbrace{9\times 20^{0}}_{9} \\
      &= J9_{20}
    \end{align*}
    $$

    ![](./source/exemple_3.png)

Si $X$ est un nombre n-aire, on le note $X_n$.


### Exemple d'autres bases

!!! example "Autres exemple de bases"
    * Base 3 = Trinaire
    * Base 8 = Octal
    * Base 9 = Nonaire
    * Base 12 = Duodécimal
    * Base 20 = Vigésimal
    * Base 60 = Sexagésimal
    * Base 150 = Indienne
    * Base d'or (puissances du nombre d'or $\frac{1 + \sqrt{5}}{2}$)
    * Base de Fibonacci (termes de la suite de Fibonacci)
    * Base factorielle (termes des factorielles)
    * ...

