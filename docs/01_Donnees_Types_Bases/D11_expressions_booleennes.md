---
author: Bruno Bourgine
title: Valeurs et expressions booléennes
hide:
  - footer
---

---
hide:
  - footer
---


!!! history "Histoire de l'informatique"
    ![](../images/portraits/George_Boole.jpg){align=left} 

    [George Boole](https://fr.wikipedia.org/wiki/George_Boole){:target="_blank"} (1815-1864) est un mathématicien et logicien britannique connu pour avoir créé la logique moderne, appelée *algèbre de Boole*.
    
    Cette algèbre binaire n'accepte que deux valeurs, 0 et 1, et a donc d'importantes et nombreuses applications en informatique...

## 1. Un peu de logique

En informatique, comme en mathématiques, on s'intéresse à la valeur de vérité de phrases ou d'expressions qui peuvent être soit vraies, soit fausses. Mais rien d'autre, c'est le principe du [tiers-exclu](https://fr.wikipedia.org/wiki/Principe_du_tiers_exclu){: target="_blank"}.

Par exemple, que diriez-vous de ces phrases?

- A: Vous êtes en classe de première.
- B: Baudelaire a écrit «Les fleurs du mal».
- C: La Terre est plate.
- D: $3 \times 4 =12$.
- E: La lettre `e` est dans le mot `abracadabra`.
- F: Georges Perec a écrit un roman de près de 300 pages sans aucune lettre `e`.
- G: $2^{10} < 10^3$
- H: La couleur orange est la plus belle des couleurs.

## 2. Algèbre de Boole

!!! abstract "Valeurs et opérations fondamentales"
    L'algèbre de Boole consiste à étudier des opérations sur un ensemble uniquement constitué de deux éléments qu'on appelle **booléens**.

    Selon le contexte (logique, calcul, électronique), ces deux éléments sont notés:

    - Faux (F) / Vrai (V)
    - 0 / 1
    - `False`/`True` (en Python, comme dans de nombreux langages)

    Les opérations fondamentales ne sont plus l'addition et la multiplication mais:

    - la **négation**, notée ¬, ou plus simplement «NON» (`not` en Python);
    - la **conjonction**, notée &, ou plus simplement «ET» (`and` en Python);
    - la **disjonction**, notée |, ou plus simplement «OU» (`or` en Python).
    
??? note "Tables de vérité"
    === "Négation, ¬, «NON», `not`"
        |x|¬x|
        |:-:|:-:|
        |F|V|
        |V|F|
        
    === "Conjonction, &, «ET», `and`"
        |x|y|x & y|
        |:-:|:-:|:-:|
        |F|F|F|
        |F|V|F|
        |V|F|F|
        |V|V|V|

    === "Disjonction, |, «OU», `or`"
        |x|y|x \| y|
        |:-:|:-:|:-:|
        |F|F|F|
        |F|V|V|
        |V|F|V|
        |V|V|V|

## 3. Avec Python

!!! note "`True` & `False`"
    - Il existe deux valeurs booléennes en Python : `True` et `False`.
    - Une variable prenant l'une de ces deux valeurs est de type `bool`.

    ```python
    >>> type(True)
    <class 'bool'>
    >>> x = False
    >>> x
    False
    >>> type(x)
    <class 'bool'>
    ```
    
!!! info inline end "Opérateurs de comparaison"
    |Opérateur|Signification|
    |:-:|:-:|
    |`==`| est égal à|
    |`!=`|est différent de|
    |`<`|inférieur à|
    |`>`|supérieur à|
    |`<=`|inférieur ou égal à|
    |`>=`|supérieur ou égal à|
    |`in`| appartient à|
    |`not in`| n'appartient pas à|

!!! note "Exemples"
    ```python
    >>> a = 2
    >>> a == 3
    False
    >>> a == 2
    True
    >>> a != 1
    True
    >>> a > 2
    False
    >>> a <= 5
    True
    >>> a % 2 == 0
    True
    >>> x = (0 == 1)
    >>> x
    False
    >>> y = (3 + 2 == 5)
    >>> y
    True
    >>> 'e' in 'abracadabra'
    False
    >>> 'b' in 'abracadabra'
    True
    >>> 'A' not in 'abracadabra'
    True
    >>> not True
    False
    >>> True and False
    False
    >>> True and True
    True
    >>> False or True
    True
    >>>
    ```

!!! done "A vous de jouer"
    Vous pouvez vérifier les expressions précédentes dans ce terminal:

    {{ terminal() }}

## 4. Les circuits logiques

!!! note "Portes logiques"

    Les transistors d'un ordinateur fonctionnent comme des **portes logiques**. Une séquence de portes logiques forme un **circuit logique**. Un circuit logique est donc un chemin emprunté par
    l'électricité permettant de faire circuler des **informations** au sein de l'ordinateur sous forme de **messages binaires**.

    Il existe 7 types de portes logiques (`NOT`, `AND`, `NAND`, `OR`, `NOR`, `XOR`, `NXOR`), comme le montre le tableau ci-dessous. Sur ce tableau :

    -   La première colonne donne le nom de la porte en français.

    -   La deuxième colonne donne l'équation (ou formule) mathématiquement
        écrite de la porte logique. Dans ces équations, `a` et `b` sont des
        chiffres binaires. Par exemple, le ET s'écrit '`.`' et le XOR
        s'écrit \( \oplus  \).

    -   Les trois colonnes suivantes montrent les représentations
        schématiques de chaque porte logique. Dans notre cours, nous
        utiliserons la notation internationale.

    -   La dernière colonne donne les tables de vérité de chaque porte,
    c'est-à-dire toutes les possibilités de valeurs de `a` et de `b` en
    entrée ainsi que la valeur `S` en sortie.


!!! info "Les 7 types de portes logiques"
    ![image](./source/portes_logiques.jpeg)

!!! tip "porte NAND"

    La porte `NAND` est une combinaison de la porte `NOT` et de la porte `AND`.  
    
    De même, la porte `NOR` est une combinaison de la porte `NOT` et de la porte `OR`
    et la porte `NXOR` est une combinaison de la porte `NOT` et de la porte `XOR`.

!!! warning "Règles importantes"

    En écriture mathématique, l'ordre de priorité est le suivant :  
    1) NOT ;  
    2) AND ;  
    3) OR et XOR.  
    Comme pour les calculs mathématiques habituels, l'utilisation de parenthèses permet de prioriser une opération.


!!! info inline end "Circuit"
    ![image](./source/circuit_logique.png)

!!! example "Exemple"

    Le schéma ci-contre est un circuit logique.  

     - En entrée du circuit on a `a=0` et `b=1`.  
     - En sortie des portes A et B, on a respectivement : \(\overline{b}\) et $\overline{a}$.  
     - En sortie des portes C et D, on a respectivement $a.b$ et $\overline{a}.\overline{b}$. 
     - Ainsi, en sortie de la porte E, on a $a.b + \overline{a}.\overline{b}$.\  
     - Le résultat final est donc $0.1 + \overline{0}.\overline{1} = 0 + 1.0 = 0 + 0 = 0$.  
