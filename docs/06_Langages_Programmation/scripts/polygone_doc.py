from turtle import forward, left, mainloop

def polygone_regulier(longueur : float, n : int) -> None:
        """
        Trace un polygone régulier à `n` côtés de longueur `longueur`
        """
        for _ in range(n):
          forward(longueur)
          angle = int(360 / n)
          left(angle)


def puissance_correction(x,k):

    resultat = 1
    for i in range(k):
        resultat = resultat*x
    return resultat
