moyenne = 7

if moyenne < 16:
    print("mention B")
elif moyenne < 12:
    print("admis")
elif moyenne < 14:
    print("mention AB")
elif moyenne < 10:
    print("repêchage")
elif moyenne < 8:
    print("raté")
else:
    print("mention TB")
