from turtle import forward, left, mainloop

def triangle_equilateral():
    for _ in range(3):
        forward(50)
        left(120)

for _ in range(5):
    triangle_equilateral()
    forward(50)

mainloop()
