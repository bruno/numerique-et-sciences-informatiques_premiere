from turtle import forward, left, mainloop

def polygone_regulier(longueur, n):
      for _ in range(n):
        forward(longueur)
        angle = int(360 / n)
        left(angle)

for i in range(3,7) :
    polygone_regulier(100,i)

mainloop()