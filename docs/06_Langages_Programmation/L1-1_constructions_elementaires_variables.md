---
author :
title : Constructions élémentaires, les variables
hide : 
 - footer
---


## 1. Les variables

### 1.1 Définition

> "Les **variables** sont des **symboles** qui associent un **nom** à une **valeur**." (Wikipédia)

!!! warning "Composantes d'une variable"

    Une variable fait référence à un **emplacement mémoire** (un conteneur)
    où est stockée une **valeur**.
    
    3 composantes définissent une variable :

    - Un **identifiant** (son nom) utilisé pour la désigner.

    - Un **type de donnée** qui caractérise la nature et le domaine de
      définition de sa valeur ainsi que les opérations que l'on peut
      effectuer sur cette variable.

    - Une **valeur** prise dans le domaine de définition associé au type.

!!! info "Modification de valeur"
    La valeur d'une variable peut changer au cours du temps (dynamique) ou
    être figée (statique, dans ce cas on dit qu'il s'agit d'une constante).

### 1.2 Déclaration d'une variable

!!! warning "Définition"

    Une **déclaration** est une instruction qui définit une variable par son
    **identifiant** et son **type de donnée**. 
    
    Il est indispensable d'avoir déclaré une variable avant de l'utiliser.

    Après sa déclaration, une variable : 

     - possède initialement une valeur qui est inconnue et aléatoire.  
     - ne peut pas changer de type de donnée.  
     - peut prendre toutes les valeurs autorisées par son type.  

!!! info "Identifiant"

    Un **identifiant** (ou nom) est une suite de caractères utilisée pour
    désigner de manière unique un élément du programme (une variable, un
    type de donnée, ...).
    
    Pour être valide (c'est-à-dire pour qu'il soit accepté), un identifiant doit :

    -   n'être composé que lettres ou chiffres ou du symbole `_`  
    -   ne pas commencer par un chiffre.

    En Python il a été décidé par convention que :

    -   L'identifiant d'une variable débute par une minuscule.  
    -   L'identifiant d'un type de donnée débute par une majuscule.  
    -   Les identifiants doivent être explicites (doivent représenter la
        variable convenablement).  
    -   Le symbole `_` permettent de séparer les mots d'un identifiant.

!!! note "déclaration en Python"

    Dans le langage Python, il n'y a pas besoin de déclarer le type d'une variable, 
    cette étape est réalisée au moment de l'affectation (voir ci-dessous). On dit 
    qu'en Python le typage est *dynamique*.

    Néanmoins il est tout de même possible de déclarer les variables avant leur
    utilisation mais cette déclaration n'a qu'un rôle informatif.

    ```python
    a: int
    t: str
    a = 42
    t = "quarante deux"
    ```


### 1.3 Affectation d'une valeur à une variable

!!! warning "Définition"
    L'**affectation** est une instruction permettant d'attribuer une valeur
    à une variable. Sa syntaxe est :  
    `<variable>` $\leftarrow$ `<valeur>`  

!!! example "Exemple : on peut affecter à une variable:"

    === "une valeur"
        $age \leftarrow 12$

    === "la valeur d'une autre variable"
        $ageDeMarius \leftarrow age$

    === "la valeur d'une expression"
        $age \leftarrow age + 1$

La valeur affectée doit être du même type de donnée que la variable.


!!! info "Affectation en Python :snake:"

    En Python, l'affectation de l'entier `12` à la variable `age` s'écrit :
    ```python
    age = 12
    ```

!!! warning "Attention"
    Le symbole `=` ici utilisé n'a **rien à voir** avec le symbole = utilisé en mathématique, il faut bien se représenter mentalement cette action par l'écriture `age ← 12`.

!!! note "Comparaison de la syntaxe d'affectation dans différents langages"

    === "Python"
        ```python
        age = 12
        ```

    === "C"
        ```c
        int age = 12;
        ```

    === "PHP"
        ```PHP
        $age = 12;
        ```

    === "Java"
        ```java
        int age = 12;
        ```

    === "Javascript"
        ```javascript
        var age = 12;
        ```

    === "Rust"
        ```rust
        let age = 12;
        ```

    === "Go"
        ```go
        age := 12
        ```

Une fois la valeur `12` stockée dans la variable `age`, on peut alors utiliser cette variable :

```python
>>> age
12
>>> age + 5
17
>>> b
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
NameError: name 'b' is not defined
```

Remarquez bien l'erreur lorsqu'on a fait appel à une variable `b` qui n'avait jamais été définie, comme le dit explicitement le message `NameError: name 'b' is not defined` .


## 2. Le fonctionnement interne
### 2.1 Explication simplifiée

En première intention, il est possible d'expliquer le fonctionnement interne de l'affectation des variables par la *métaphore des tiroirs* :


Écrire  l'instruction :
```python
>>> a = 2
```

va provoquer chez l'ordinateur le comportement suivant :

- Est-ce que je possède **déjà** un tiroir appelé ```a``` ? 
    - si oui, je me positionne devant.
    - si non, je crée un tiroir appelé ```a```.
    - J'ouvre le tiroir et j'y dépose la valeur numérique 2. Si le tiroir contenait déjà une valeur, celle-ci disparaît (on dit qu'elle est **écrasée**).

![illustration métaphore](https://glassus.github.io/premiere_nsi/T1_Demarrer_en_Python/1.1_Variables/data/tiroirs.png){: .center}

Cette explication est suffisante pour aborder la notion de variable : c'est un mot (ou une lettre) qui va désigner une valeur. 

### 2.2 Une histoire en 2 temps : évaluation, affectation

Observons l'instruction écrite en Python :

```python
>>> a = 2 + 3
```

#### Étape 1 : **l'évaluation**

Python va prendre la partie à droite du signe égal et va l'évaluer, ce qui signifie 
qu'il va essayer de lui donner une valeur. Dans nos exemples, cette valeur sera 
numérique, mais elle peut être d'un autre type (voir plus loin)

Ici, Python effectue le calcul `2 + 3` et l'évalue à la valeur `5`.

#### Étape 2 : **l'affectation**

Une fois évaluée l'expression à **droite** du signe `=,` il ne reste plus qu'à l'affecter
 à la variable (déjà existante ou pas) située à **gauche** du signe`=`.

Comme expliqué précédemment, un «lien» est fait entre le nom de la variable et 
l'adresse-mémoire qui contient la valeur évaluée.

`a` sera donc lié à la valeur `5`. Plus simplement, on dira que «`a`` vaut `5`» 

### 2.3 L'incrémentation d'une variable.

!!! warning "Définition"
        *«Incrémenter»* une variable signifie l'augmenter. 

Imaginons une variable appelée ```compteur```. Au démarrage de notre programme, elle est
 initialisée à la valeur 0. 

```python
compteur = 0
```
Considérons qu'à un moment du programme, cette variable doit être modifiée, par exemple 
en lui ajoutant 1.

En pseudo-code, cela s'écrira :  

$compteur \leftarrow compteur + 1$

En Python, cela s'écrira :

```python
compteur = compteur + 1
```

Observée avec des yeux de mathématicien, la précédente instruction est une horreur.

![illustration x=x+1](https://glassus.github.io/premiere_nsi/T1_Demarrer_en_Python/1.1_Variables/data/memex.png){: .center width=30%}

!!! note "Déroulement"

    Vue avec des yeux d'informaticien, voilà comment est interprétée la commande :

    ```python
    compteur = compteur + 1
    ```

    1. On évalue la partie droite de l'égalité, donc l'expression ```compteur + 1```.
    2. On va donc chercher le contenu de la variable ```compteur```. Si celle-ci n'existe 
    pas, un message d'erreur est renvoyé.
    3. On additionne 1 au contenu de la variable ```compteur```.
    4. On écrase le contenu actuel de la variable ```compteur``` avec la valeur obtenue 
    au 3.   

    À la fin de ces opérations, la variable ```compteur``` a bien augmenté de 1.

!!! warning "À maîtriser"
    Cette procédure d'**incrémentation** est très très classique, il faut la maîtriser 
    parfaitement !

!!! info "Syntaxe classique et syntaxe Pythonesque :heart:"
    L'incrémentation d'une variable ```compteur``` s'écrira donc en Python :
    ```python
    >>> compteur = compteur + 1
    ```
    Mais il existe aussi une syntaxe particulière, un peu plus courte :

    ```python
    >>> compteur += 1
    ```
    Cette syntaxe peut se ranger dans la catégorie des **sucres syntaxiques** : c'est bien de la connaître, c'est amusant de s'en servir, mais son utilisation n'est en rien obligatoire et peut avoir un effet néfaste, celui d'oublier réellement ce qu'il se passe derrière.

!!! example "{{ exercice() }}"
    === "Énoncé"
        Écrire le code «classique» et le code «Pythonesque» pour l'instruction suivante :

        On initialise une variable ```score``` à 100 et on l'augmente de 15.

        ??? example "Terminal"

            {{ terminal() }}

    === "Correction"
        ```python
        >>> score = 100
        >>> score = score + 15
        ```
        ou bien
        ```python
        >>> score = 100
        >>> score += 15
        ```


!!! example "{{ exercice() }}"
    === "Énoncé"
        Écrire le code «classique» et le code «Pythonesque» pour l'instruction suivante :

        On initialise une variable ```cellule``` à 1 et on la multiplie par 2.

        ??? example "Terminal"

            {{ terminal() }}

    === "Correction"
        ```python
        >>> cellule = 1
        >>> cellule = cellule * 2
        ```
        ou bien
        ```python
        >>> cellule = 1
        >>> cellule *= 2
        ```

!!! example "{{ exercice() }}"
    === "Énoncé"
        Écrire le code «classique» et le code «Pythonesque» pour l'instruction suivante.

        On initialise une variable ```capital``` à 1000 et on lui enlève 5%.

        ??? example "Terminal"

            {{ terminal() }}

    === "Correction"
        ```python
        >>> capital = 1000
        >>> capital = capital - capital * 5/100
        ```
        ou bien
        ```python
        >>> capital = 1000
        >>> capital *= 0.95
        ```


#### L'échange de variables

Après l'incrémentation, une autre technique de base reviendra fréquemment dans nos codes : **l'échange de variables**.

Imaginons les variables suivantes :

```python
>>> a = 3
>>> b = 5
```
Le but est d'échanger les valeurs de ```a``` et de ```b```.

▸ **Méthode naïve**

```python
>>> a = b
>>> b = a
```

Que valent ```a``` et ```b``` maintenant ?

Malheureusement :
```python
>>> a
5
>>> b
5
>
```

La variable ```a``` a été écrasée dès qu'on lui a donné la valeur de la variable ```b```.

Comment la préserver ?

La situation est similaire au problème suivant : comment échanger le contenu de ces deux verres ?

![image](https://glassus.github.io/premiere_nsi/T1_Demarrer_en_Python/1.1_Variables/data/verres.png){: .center width=20%}

La méthode est évidente : il nous faut un troisième verre.

Nous allons faire de même pour nos variables. Nous allons utiliser une variable **temporaire** (on parle aussi de variable **tampon**) pour conserver la mémoire de la valeur de ```a``` (par exemple) avant que celle-ci ne se fasse écraser :

```python
>>> a = 3
>>> b = 5
>>> temp = a
>>> a = b
>>> b = temp
```

Vous pouvez vérifier maintenant que les valeurs de ```a``` et de ```b``` ont bien été échangées.


!!! info "Syntaxe classique et syntaxe Pythonesque :heart:"
    L'échange de deux variables ```a``` et de ```b``` s'écrit donc :
    ```python
    >>> temp = a
    >>> a = b
    >>> b = temp
    ```
    Mais il existe aussi une syntaxe particulière à Python, bien plus courte :

    ```python
    >>> a, b = b, a
    ```

    C'est de nouveau un *sucre syntaxique*. Cette syntaxe nous dispense de créer 
    nous-même une troisième variable. Mais pas de miracle : en interne, Python crée
    lui-même cette variable temporaire. La simultanéité n'existe pas en informatique.

!!! example "{{ exercice() }}"

    === "Énoncé"
        Une petite erreur s'est glissée à Poudlard :
        ```python
        >>> maison_Harry = "Serpentard"
        >>> maison_Malfoy =  "Gryffondor"
        ```
        Corriger cette erreur, de deux manières différentes.

        ??? example "Terminal"

            {{ terminal() }}

    === "Correction"
        ```python
        >>> t = maison_Harry
        >>> maison_Harry = maison_Malfoy
        >>> maison_Malfoy = t
        ```
        ou plus rapidement :
        ```python
        >>> maison_Harry, maison_Malfoy = maison_Malfoy, maison_Harry
        ```



## 3. Différents types de variables



Pour différencier la nature de ce que peut contenir une variable, on parle alors de 
**type de variable**.

En voici quelques uns, que nous découvrirons au fil de l'année :


!!! abstract "Types de base"

    Voici les types Python que nous rencontrerons cette année:

    |Type Python| Traduction | Exemple|
    |:-:|:-:|:-:|
    |`int`|entier|`#!python 42`|
    |`float`|flottant (décimal)|`#!python 3.1416`|
    |`str`|chaîne de caractères (string)|`#!python "NSI"`|
    |`bool`|booléen (True ou False)|```#!python True```|
    |`tuple`|p-uplet| `#!python (255, 127, 0)`|
    |`list`|liste|`#!python [0, 1, 2, 3, 4, 5]`|
    |`dict`|dictionnaire|`#!python {'Homer':43, 'Marge':41, 'Bart':12, 'Lisa':10, 'Maggie':4}`|
    |`function`|fonction| `#!python print`|


Comment connaître le type d'une variable ?
Il suffit dans la console d'utiliser la fonction `type`.

```python
>>> a = 1
>>> type(a)
<class 'int'>
```

## 4. Python et le typage dynamique

Nous avons vu qu'il n'était pas utile de préciser à Python le type de notre variable.

```python
a = 3
```

Mais dans certains langages, c'est obligatoire. En C par exemple, il faut écrire :

```C
int a = 3;
```
Cela signifie (pour le langage C) que notre variable ```a``` n'aura pas le droit de contenir autre chose qu'un nombre entier.

Si on écrit ensuite
```C
a = "test";
```

Le compilateur C renverra une erreur : on ne peut pas stocker une chaîne de caractères dans une variable qu'on a créée comme étant de type entier.

Et en Python ?

```python
>>> a = 3
>>> type(a)
<class 'int'>
>>> a = "test"
>>> type(a)
<class 'str'>
```

Python a changé tout seul le type de notre variable, sans intervention. On parle de 
**typage dynamique**.

!!! info "Déclaration de type en Python"

    Même si nous déclarons le type d'une variable en Python comme vu dans un exemple
    plus haut, l'exécution du programme ne renverra pas d'erreur si nous affectons à 
    une variable une donnée d'un autre type que celui annoncé. 

    ```python
    a : int
    a = "texte"
    ```
    En Python, le fait d'indiquer le type est purement informatif, il faut bien avoir 
    conscience que la ligne `a : int` ne permet pas de créer la variable `a` :

    ```python
    >>> a :int
    >>> type(a)
    Traceback (most recent call last):
        File "<stdin>", line 1, in <module>
    NameError: name 'a' is not defined
    ```
    
       
Référence:

- G.Lassus (lycée François Mauriac, Bordeaux), [Première NSI - Variables](https://glassus.github.io/premiere_nsi/T1_Demarrer_en_Python/1.1_Variables/cours/)