---
author :
title : Constructions élémentaires, les boucles NON bornées
hide : 
 - footer
---


## 1. Quelques problèmes pour commencer

!!! question "Problème 1" 

    === "Question"

        Écrire un algorithme qui affiche le plus petit nombre entier $n$ tel que $2^n$ soit 
        supérieur à 1 milliard.

    === "Algorithme en Pseudo-Code"

        ```        
        Déclarations
            n : Entier
        Début
            n ← 1
            Tant que 2**n < 10**9 , faire
                n ← n + 1
            FinTantque
        Fin
        ```
    
    === "Implémentation en Python :snake:"

        ```python linenums='1'
        n = 1
        while 2**n < 10**9:
            n = n + 1
        print(n)
        ```


!!! question "Problème 2" 

    === "Question"

        Écrire un algorithme qui demande la saisie d'une lettre `O` ou `N` et qui continue
         à demander si la lettre saisie n'est ni `O`, ni  `N`.

    === "Algorithme en Pseudo-Code"

        ```        
        Déclarations
            l : chaîne de caractères
        Début
            l ← Lire("Saisir `O` ou `N`")
            Tant que l ≠ `O` et l ≠ `N`, faire
                l ← Lire("Saisir `O` ou `N`")
            FinTantque
        Fin
        ```
    
    === "Implémentation en Python :snake:"

        ```python linenums='1'
        lettre = input("Saisir O ou N")
        while lettre != "O" and lettre != "N":
            lettre = input("Saisir O ou N")
        ```

    
## 2. Une autre façon de répéter 

!!! warning "Différence entre boucle bornée et non-bornée"

    À la différence essentielle des boucles bornées `Pour` (ou `for`), dont on peut savoir 
    à l'avance combien de fois elles vont être exécutées, les boucles non-bornées `Tant que`
     (ou `while`) sont des boucles dont on ne sort que lorsqu'une condition n'est plus satisfaite. 

!!! danger "C'est risqué !"
    
    Le risque en utilisant une boucle Tant que est de rester infiniment bloqué à l'intérieur !  

!!! note "Exemple n°1 "

    Le programme suivant :
    ```python linenums='1'
    a = 0
    while a < 3:
        print("ok")
        a = a + 1
    print("fini")
    ```
    va donner ceci :
    ```python
    ok
    ok
    ok
    fini

    ```
!!! aide "Analyse grâce à PythonTutor"
    <iframe width="800" height="300" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=a%20%3D%200%0Awhile%20a%20%3C%203%3A%0A%20%20%20%20print%28%22ok%22%29%0A%20%20%20%20a%20%3D%20a%20%2B%201%0Aprint%28%22fini%22%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>


!!! question
    le code ci-dessous va-t-il donner un résultat différent ?
    ```python linenums='1'
    a = 0
    while a < 3:
        a = a + 1
        print("ok")
    print("fini")
    ```

??? info "Vérification "

    Vérifier votre réponse grâce à l'DE ci-dessous.

    {{ IDEv() }}
    

!!! warning "Évaluation de la condition"

    **Conclusion** : l'évaluation de la condition ne se fait pas à chaque ligne mais bien au début 
    de chaque tour de boucle. Si la variable qui déclenchera la sortie de boucle atteint sa valeur 
    de sortie au milieu des instructions, les lignes restantes sont quand même exécutées.


## 3. Boucle non-bornée

### 3.1 L'instruction `Tant que`

!!! abstract "Syntaxe"

    === "En Pseudo-Code"

        ```
        Tant que <condition>, faire
            instruction1
            instruction2
            ...
            instructionN
        FinTantque
        ```

    === "En Python :snake:"

        ```python
        while condition:
            instruction1
            instruction2
            ...
            instructionN
        ```

    === "En C"

        ```C
        while (test) {
        instruction1;
        instruction2;
        ...
        instructionN
        }
        ```

    === "En Javascript ☕"

        ```js
        while (test) {
        instruction1;
        instruction2;
        ...
        instructionN
        }
        ```
            

### 3.2 La condition

!!! warning ""

    La ```condition``` est un booléen, c'est-à-dire une expression que Python évaluera à ```True``` 
    ou à ```False```.

Exemple de booléens résultant d'une évaluation :
```python
>>> 1 < 3
True
>>> 5 > 7
False
>>> a = 10
>>> a > 8
True
```

**Rappel** : le cours sur les booléens est disponible [ici](../../01_Donnees_Types_Bases/D11_expressions_booleennes).

### 3.3 Les instructions

Les instructions ```instruction1``` jusqu'à ```instructionN``` sont exécutées dans cet ordre à 
chaque tour de boucle. 


!!! warning "Terminaison"

    **Attention :** pour que la boucle s'arrête, les instructions doivent obligatoirement avoir un
     impact sur la ```condition``` évaluée après le ```while```, de façon à ce qu'elle devienne fausse.
    
    Pour prouver qu'un programme comportant une telle boucle termine, la technique de raisonnement 
    utilisée est celle du **variant**. Celle-ci consiste à trouver parmi les instructions une quantité
     qui est entière et positive et qui décroît strictement à chaque tour de boucle.


## 4. Les pièges ...

### 4.1 piège n°1 : ne JAMAIS SORTIR de la boucle


!!! bug "Exemple n°1 "

    Le programme suivant :
    ```python linenums='1'
    a = 0
    while a < 3:
        print("ok")
        a = a + 1
        a = a * 0
    print("ce texte ne s'écrira jamais")
    ```

    va écrire une suite infinie de ```ok``` et ne **jamais s'arrêter**. Un variant de boucle 
    pourrait être ici `3-a`, et celui-ci pourrait effectivement décroître s'il n'y avait pas
    l'instruction `a = a * 0`.


### 4.2 piège n°2 : ne JAMAIS ENTRER dans la boucle

!!! bug "Exemple n°2 "

    Le programme suivant :
    ```python linenums='1'
    a = 0
    while a > 10:
        print("ce texte non plus ne s'écrira jamais")
        a = a + 1
        
    print("fini") 
    ```

    va écrire ```fini``` et s'arrêter.



## 5. Quelques remarques

### 5.1 Lien entre ```while``` et ```for```

![image](res/scooby.png){: .center width=40%}

La boucle bornée ```for``` que nous avons étudiée est très pratique.

Mais nous pourrions nous en passer : toutes les boucles ```for``` peuvent en fait être ré-écrites en utilisant 
```while```. (alors que la réciproque est fausse)


!!! example "Exercice "

    === "Énoncé"
        On considère le code ci-dessous :
        ```python linenums='1'
        for k in range(5):
            print("scooby-doo")
        ``` 
        Ré-écrire ce code en utilisant une boucle ```while```.

    === "Correction"

        {{ IDEv() }}

### 4.2 Les boucles infinies volontaires
 
La boucle infinie a été présentée comme un danger qu'il faut éviter. 

Pourtant, dans quelques situations, il est d'usage d'enfermer _volontairement_ l'utilisateur 
dans une boucle infinie.

C'est notamment le cas des codes Processing (ou p5) où la fonction ```draw()``` est une boucle 
infinie dont on ne sort que lorsqu'un évènement est intercepté (par exemple, le clic sur la 
fermeture de la fenêtre d'affichage).

C'est aussi le cas pour les cartes à micro-contrôleur telle que `micro:bit`. On inclut le code 
dans une boucle infinie afin que le programme s'exécute en continu sur la carte et que celle-ci 
puisse réagir lors du déclenchement d'un événement (un appui sur un bouton par exemple).

!!! example "Observez et exécutez le code suivant"

    ```python
    while True :
    reponse = input("tapez sur la lettre S du clavier pour me sortir de cet enfer : ")
    if reponse == 'S' or reponse == 's':
        break

    print("merci, j'étais bloqué dans une boucle infinie")
    ```

    {{ IDE('while1') }}

    - le début du code : ```while True``` est typique des boucles infinies volontaires. On aurait tout aussi bien pu écrire ```while 3 > 2``` (on rencontre même parfois des ```while 1```)  
    - vous avez découvert l'expression ```break``` qui comme son nom l'indique permet de casser la boucle (cela marche pour ```while``` comme pour ```for```) et donc d'en sortir. Son emploi est controversé parmi les puristes de la programmation. Nous dirons juste que c'est une instruction bien pratique.  

!!! example "Exercice"

    === "Énoncé"

        Proposer un code qui choisit un nombre aléatoire entre 1 et 10, puis qui propose en boucle à l'utilisateur de le deviner, tant que celui-ci n'a pas trouvé.

        Aides :

        - ```int()``` permet de convertir une chaîne de caractères en nombre. 
        - pour avoir un nombre ```a``` pseudo-aléatoire :
        ```python
        from random import randint
        a = randint(1,10)
        ```

    === "Correction"
       

        {{ IDE ()}}
<!--         
        ```python linenums='1'
        from random import randint

        mystere = randint(1, 10)

        while True:
            reponse = int(input('quel est le nombre mystère ? '))
            if reponse == mystere:
                break
        print('bravo !')
        ``` -->
        
        

*Références* :

 - T.Balabonski, S.Conchon, JC.Filliâtre, K.Nguyen  *Numérique et Sciences Informatiques*, Ed. Ellipses
 - G.Lassus (lycée François Mauriac, Bordeaux), [Première NSI - Base de Python, Boucle While](https://glassus.github.io/premiere_nsi/T1_Demarrer_en_Python/1.3_Boucle_while/cours/)