---
author :
title : Constructions élémentaires, les boucles bornées
hide : 
 - footer
---

## 1. Quelques problèmes

### 1.1 Les *n* premiers entiers <a name="pb1"></a>

!!! question "Problème 1" 

    === "Question"

        Comment écrire un algorithme qui demande à l’utilisateur un nombre
        entier n, et qui affiche les n premiers entiers ?

    === "Algorithme en Pseudo-Code"

        ```
        Algorithme d’affichage des n premiers entiers
        Déclarations
            n, i : Entier
        Début
            Écrire("Quel est l’entier ? ")
            Lire(n)
            Pour i ← 1 à n faire
                Écrire(i)
            FinPour
        Fin
        ```
    
    === "Implémentation en Python :snake:"

        ```python
        n=int(input("Quel est l’entier ? "))
        for i in range(n):
            print(i)
        ```




### 1.2 Multiplier sans l'opérateur de multiplication <a name="pb2"></a>

!!! question "Problème 2"

    === "Question"

        Comment écrire un algorithme qui demande à l'utilisateur un nombre réel puis un nombre entier
        et effectue la multiplication de ces nombres sans utiliser l'opérateur de multiplication ?

    === "Algorithme en Pseudo-Code"

        ```
        Algorithme de multication entre un réel et un entier
        Déclarations
            r,s : Réel
            n,i : Entier
        Début
            Écrire("Quel est le réel ? ")
            Lire(r)
            Écrire("Quel est l’entier ? ")
            Lire(n)
            s ← r
            Pour i ← 1 à n faire
                s ← s + r                
            FinPour
            Écrire(s)
        Fin
        ```
    
    === "Implémentation en Python :snake:"

        ```python
            n = int(input("Quel est l’entier ? "))
            r = float(input("Quel est le réel ? "))
            sortie = r
            for _ in range(n-1):
                sortie = sortie + r
            print(sortie)
        ```

### 1.3 Dessiner une spirale

!!! question "Problème 3"
    === "Question"

        Comment dessiner cette spirale en python (à partir du module turtle) ?  

        ![spirale](res/prog-spirale.png)

        Le module `turtle` permet de tracer des demi-cercle avec l'instruction `circle(diamètre, angle)`,
         la modification de l'épaisseur du trait se fait quant à elle avec l'instruction `width(épaisseur)`.

        L'instruction ci-dessous fixe l'épaisseur du trait à 2 puis trace un demi-cercle (angle = 180°) de
        diamètre 10; ce qui peut constituer la première étape de construction de notre spirale.

        ```python
        from turtle import width, circle    #importation des fonctionnalités nécessaires

        width(2)
        circle(10, 180)
        ```

    === "Implémentation en Python :snake: SANS boucle `for`"

        ```python
        from turtle import width, circle    #importation des fonctionnalités nécessaires

        width(2)
        circle(10, 180)
        width(3)
        circle(20, 180)
        width(4)
        circle(30, 180)
        width(5)
        circle(40, 180)
        width(6)
        circle(50, 180)
        width(7)
        circle(60, 180)
        width(8)
        circle(70, 180)
        ```

    === "Implémentation en Python :snake: AVEC boucle `for`"
<!-- 
        ```python
        from turtle import width, circle    #importation des fonctionnalités nécessaires

        epaisseur = 2
        diametre = 10
        for i in range(8):
            width(epaisseur)
            circle(diametre, 180)
            epaisseur = epaisseur + 1
            diametre = diametre + 10
        ``` -->

### À RETENIR

!!! warning "Instructions itératives bornées"

    Une **itération** est une suite d’instructions dont l’exécution s’effectue plusieurs fois. 
    On utilise aussi le terme de boucle. La condition d’arrêt de la boucle indique quand l’itération
    doit recommencer une exécution ou “quitter” la boucle.

    Lorsqu’on utilise une **boucle bornée**, le nombre d’itérations est défini au début de la boucle.  
    La variable de l’itération est de n’importe quel type dénombrable (en mathématiques, on dit qu'un 
    ensemble est dénombrable lorsqu'on peut associer à chaque élément de l'ensemble un nombre).

    En Pseudo-code, sa syntaxe est :  
    ```
    Pour <variable> ← <valeur début> à <valeur fin> faire
        <bloc d’instructions>
    FinPour
    ```

    En python la syntaxe d'une boucle bornée est `#!python for ... in ... :` 


## 2. Répéter avec une boucle bornée simple <a name="simple"></a>

En Python, la façon la plus simple de répéter une instruction, par exemple `#!python print("Bonjour")` 
est la suivante :

`#!python for _ in range(3) : print("Bonjour")`

L'utilisation du caractère `_` , plutôt qu'un caractère alphabétique a une signification particulière 
sur laquelle nous reviendrons par la suite.

!!! danger "Indentation et bloc d'instructions"

    L'instruction à répéter est située après les deux-points `:` et constitue le **corps** de la boucle. 
    Si le corps de la boucle comporte plusieurs instructions, il faudra nécessairement les regrouper dans un *bloc*.  

    Un *bloc* est consitué par une suite de lignes comportant le même retrait : on parle d'**indentation**. L'usage est d'utiliser quatre espaces ce qui correspond la plupart du temps à une tabulation.  
     
    En Python, l'**indentation** joue un rôle très important dans la signification du code.

    On peut faire le parallèle entre l'usage de l'indentation en Python et l'utilisation de blocs dans le langage Scratch.

    !!! example inline end "En Scratch"
        
    ![](res/scratchblocks_boucle.png){width=60%/linewidth}

            est différent de :

![](res/scratchblocks_boucle2.png){width=60%/linewidth}



    !!! example "En Python"  

    ```python        
    for _ in range(3):
        n = n + 1
        print(n)
    ```

    est différent de :

    ```python
    for _ in range(3):
        n = n + 1
    print(n)
    ```

Le nombre de tours de boucle peut être donnée par une variable, ou par une expression arithmétique,
 il faut néanmoins veiller impérativement à ce que ce soit **un nombre entier**.

Le corps de boucle peut recevoir tout type d'instructions, y compris d'autres boucles, on parle 
alors de **boucles imbriquées**. On en reparlera dans un autre chapitre. 

## 3. Exploiter l'indice de boucle

Lorsque l'on écrit une boucle bornée, on utilise une variable pour énumérer les tours de boucle. 
Il est possible d'utiliser cette variable à l'intérieur du corps de boucle, comme on l'a vu avec 
le [problème n°1](#pb1).

```python
    n = int(input("Quel est l’entier ? "))
    for i in range(n):
        print(i)
```    

!!! warning "Indice de boucle"

    Cette variable s'appelle **indice de boucle** ou **compteur de boucle**. 
    
    Dans l'exemple précédent il s'agit de la variable `i` (comme **itération**). Bien entendu il est 
    tout à fait possible de la nommer autrement selon les besoins.

Dans le cas d'une [boucle bornée simple](#simple) il est d'usage de préférer le caractère `_` en lieu et place
d'une lettre, cela permet de signifier que cette variable est muette : on ne fera pas appelle à elle dans le corps 
de la boucle.

!!! danger "Numérotation des tours"
    Le numéro du premier tour est 0, celui du deuxième est donc 1, etc... 
    
    Ainsi le numéro du dernier tour sera un de moins que le nombre total de tour.

!!! danger "Portée de l'indice de boucle"
    Il ne faut pas utiliser pour une variable de boucle un nom qui est déjà utilisé pour une autre variable.  
    Une variable `i` qui sert de compteur dans une boucle `for` existe encore après l'exécution de la boucle et
     si une valeur a été affectée à `i` auparavant, elle sera écrasée lors de l'exécution de la boucle.  
    Cliquer sur le bouton "lecture" de l'IDE ci-dessous pour vous en convaincre.

    {{ IDEv('scripts/boucle_for')}}

## 4. Utiliser une variable dans une boucle

Dans le cas du [problème n°2](#pb2), nous avons utilisé une variable autre que l'indice de boucle
pour enregistrer un résultat intermédiaire. À chaque tour de boucle la valeur de cette variable était 
mise à jour. Cette variable dont la valeur progresse à chaque tour constitue dans ce cas ce qu'on
appelle un **accumulateur**.

!!! example "Usage d'un accumulateur"  

    Un bon exemple d'utilisation d'un accumulateur est le calcul d'une moyenne, où l'on a besoin de
    cumuler l'ensemble des valeurs des notes avant de diviser la somme obtenue par le nombre de 
    valeurs.

    En voici une implémentation en Python  :  
    
    ```python
    n = int(input("Entrer le nombre d'élèves : "))
    somme = 0
    for _ in range(n):
        note = int(input("Entrer une note : "))
        somme = somme + note
    moyenne = somme / n
    print("La moyenne est :", moyenne)
    ```

## 5. Dérouler l'éxécution

<iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=s%3D0%0An%20%3D%204%0Afor%20i%20in%20range%28n%29%3A%0A%20%20%20%20z%3Di**2%0A%20%20%20%20s%3Ds%2Bz%0Aprint%28%22Somme%20des%20%22,n,%22%20premiers%20carr%C3%A9s%20%3A%20%22,s%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

## 6. Itérer autrement

### 6.1 Compléments sur `range()`

Jusqu'à présent nous avons utilisé l'instruction range() avec un seul paramètre indiquant le 
nombre de tours à effectuer. Cette instruction peut être enrichie.  

La syntaxe complète est `#!python for i in range(début, fin, pas)`, telle que:

- `début` donne l'indice du premier tour;  
- les tours s'arrêtront avant d'atteindre ou de dépasser `fin`;  
- à chaque tour l'indice est augmenté de la valeur `pas`.  

!!! tldr "Exercice"

    Écrire dans l'IDE ci-dessous un programme utilisant la boucle `for` et affichant les multiples 
    de 5 entre -20 et 20 inclus.  

    {{ IDEv ()}}

### 6.2 Itérer sur des itérables

!!! warning "Itérable"

    En informatique, il existe un concept qui va désigner les objets que l'on peut énumérer, 
    c'est-à-dire les décomposer en une succession ordonnée d'éléments. On les appelle les 
    énumérables ou les itérables (Python utilise le mot anglais *iterable*).

Avec l'instruction `#!python range(...)` , on crée un itérable que l'on peut assimiler 
(de façon simpliste) à une liste de nombre. C'est cette "liste" que la boucle 
`#!python for ... in ...` va parcourir.

Mais on peut obtenir des itérables autrement qu'avec `range` :  

- la variable `NSI` (qui est de type `String`) est énumérable : on peut la décomposer
  en `N`, `S`, `I`.  
- la variable `[4, 3, 17]` (qui est de type `List`) est énumérable : on peut la décomposer 
  en `4`, `3`, `17`.  

Par contre la variable `5` (qui est de type `Int`) n'est PAS énumérable : on ne peut pas la 
décomposer.

!!! example "Itérables en actions"

    {{ IDEv('scripts/iterable')}}


---

*Références*:

- G.Lassus (lycée François Mauriac, Bordeaux), [Première NSI - Boucles for](hhttps://glassus.github.io/premiere_nsi/T1_Demarrer_en_Python/1.2_Boucle_for/cours/)
- T.Balabonski, S.Conchon, JC.Filliâtre, K.Nguyen  *Numérique et Sciences Informatiques*, Ed. Ellipses


