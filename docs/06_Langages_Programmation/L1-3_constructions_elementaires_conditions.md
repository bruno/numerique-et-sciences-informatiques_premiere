---
author :
title : Constructions élémentaires, conditions.
hide : 
 - footer
---

## 1. Quelques problèmes pour commencer

### 1.1 Pile ou face <a name="pb1"></a>

!!! question "Problème 1" 

    === "Question"

        Comment écrire un algorithme qui affiche "Pile" ou "Face" selon le résultat d'un tirage
        aléatoire d'un entier entre 0 et 1.

    === "Algorithme en Pseudo-Code"

        ```        
        Déclarations
            tirage : Entier
        Début
            tirage ← entier aléatoire entre 0 et 1
            Si tirage = 0 , alors
                Écrire("Pile")
            Sinon
                Écrire("Face")
            FinSi
        Fin
        ```
    
    === "Implémentation en Python :snake:"

        ```python
        from random import randint
        n = randint(0,1)
        if n==0:
            print("Pile")
        else :
            print("Face")
        ```

### 1.2 Lancé de dé <a name="pb2"></a>

!!! question "Problème 2" 

    === "Question"

        On décrit la règle du jeu suivante :   

         - on lance un dé à 6 faces,
         - si le résultat est inférieur ou égal à 2, on perd 1 point,  
         - si le résultat est égal à 6, on gagne 2 points.  


        Proposer un algorithme qui affiche le score après un tour de jeu, 
        on débutera avec un score égal à 2.

    === "Algorithme en Pseudo-Code"

        ```        
        Déclarations
            score,tirage : Entier
        Début
            score ← 2
            tirage ← entier aléatoire entre 1 et 6
            Si tirage < 3 , alors
                score ← score - 1            
            Sinon Si tirage = 6 , alors
                score ← score + 2             
            FinSi
            Écrire(score)
        Fin
        ```
    
    === "Implémentation en Python :snake:"

        ```python
        from random import randint
        tirage = randint(1,6)
        score = 2
        if n <= 2 :
            score = score - 1
        elif n == 6 :
            score = score + 2
        print(score)
        ```

## 2. Branchement conditionnel

En programmation il est très courant de devoir effectuer des instructions différentes selon les cas de figures 
rencontrés. Une instruction de *branchement* permet de choisir un bloc de code à exécuter plutôt qu'un autre, selon
une condition définie au préalable.

### 2.1 L’instruction **Si Alors** .

Cette instruction permet de conditionner l’exécution d’une instruction ou
d'un bloc de code à la valeur d’une expression booléenne. En Pseudo-code, sa syntaxe est :

```
Si <expression booléenne> Alors
<instructions si vrai>
Fin si
```

!!! example "Exemple : nombre positif ?"

    === "Algorithme en Pseudo-Code"

        ```
        Si n > 0 Alors
            Écrire("le nombre", n,"est positif")
        Fin si
        ```

    === "Implémentation en Python :snake:"

        ```python
        if n > 0 :
            print("le nombre", n,"est positif")
        ```

!!! warning "Indentation"
    Comme pour l'instruction de boucle ` ... for :` le bloc d'instruction à
    exécuter doit être placé en retrait après le symbole `:`

### 2.2 L’instruction **Si Alors Sinon**.

En plus d'un bloc à n'exécuter que lorsque la condition est vérifiée, une instruction
 **Si** peut contenir un bloc alternatif, à n'exécuter que dans le cas contraire.

En Pseudo-code, sa syntaxe est :  

```
Si <expression booléenne> Alors
<instructions si vrai>
Sinon
<instructions si faux>
Fin si
```

En Python, le mot-clé correspond à `Sinon` est ... `else`.


!!! example "Exemple : nombre positif ou négatif (ou nul)?"

    === "Algorithme en Pseudo-Code"

        ```
        Si n > 0 Alors
            Écrire("le nombre", n,"est positif")
        Sinon
            Écrire("le nombre", n,"est négatif ou nul")
        Fin si
        ```

    === "Implémentation en Python :snake:"

        ```python
        if n > 0 :
            print("Le nombre", n,"est positif")
        else :
            print("Le nombre", n,"est négatif ou nul")
        ```
!!! warning "Indentation ... encore"
    Le bloc introduit par `else :` doit lui aussi être indenté, et
    son identation doit s'aligner sur le bloc d'instruction précédent.

### 2.3 L’instruction **Si Alors Sinon Si**.

Il est possible de prévoir plus que deux branchements conditionnels. Après une première
branche conditionnelle **Si**, chaque branche suivante peut être ajoutée avec sa propre 
condition grâce aux mots clés **Sinon si**.

En Pseudo-code, sa syntaxe est :  

```
Si <condition1> Alors
    <instructions si condition1 vraie>
Sinon si <condition2> Alors
    <instructions si condition2 vraie>
Sinon
    <instructions si condition1 et condition2 fausses>
Fin si
```

En Python, le mot-clé correspond à `Sinon si` est `elif` qui est la contraction de `else` et `if`.

!!! example "Exemple : nombre positif ou négatif?"

    === "Algorithme en Pseudo-Code"

        ```
        Si n > 0 Alors
            Écrire("le nombre", n,"est positif")
        Sinon si n = 0 Alors
            Écrire("le nombre", n,"est nul")
        Sinon
            Écrire("le nombre", n,"est négatif")
        Fin si
        ```

    === "Implémentation en Python :snake:"

        ```python
        if n > 0 :
            print("Le nombre", n,"est positif")
        elif n==0 :
            print("Le nombre", n,"est nul")
        else :
            print("Le nombre", n,"est négatif")
        ```
!!! danger "Attention à l'ordre"
    L'ordre dans lequel apparaissent les conditions compte : seule la première 
    branche dont la condition est vraie est exécutée. 

    Observer ainsi l'exécution du programme ci-dessous. Corriger-le afin que 
    l'affichage soit cohérent.

    {{ IDEv ('scripts/examen')}}


### 2.4 Jonction

Après l'exécution d'une instruction comportant un branchement conditionnel, 
l'exécution du programme se poursuit avec l'instruction suivante, quel que soit 
la branche qui a été sélectionnée. On dit qu'il y a jonction des différentes 
branches.

!!! example "Positif ou Négatif (ou nul)"
    Étudions le déroulement du programme ci-dessous :  

    ```python
        print("Début")
        if n > 0 :
            print("Strictement positif")
        else :
            print("Négatif ou nul")
        print("Fin")
    ```

    ``` mermaid
    graph LR
    . --> A("print('Début')") ;
    A -->|n>0| B("print('Strictement positif')");
    A -->|n <= 0 | C("print('Négatif ou nul')");
    C --> D("print('Fin')");
    B --> D("print('Fin')");
    D --> ..
    ```

    Ce type de diagramme s'appelle un graphe de contrôle de flot et permet de schématiser les 
    branchements et les jonctions.


!!! example "Positif ou Négatif (ou nul)"
    Étudions le déroulement du programme ci-dessous :  

    ```python
        print("Début")
        if n > 0 :
            print("Positif")
        elif n==0 :
            print("Nul")
        else :
            print("Négatif")
        print("Fin")
    ```

    ``` mermaid
    graph LR
    .. --> A("print('Début')") ;
    A -->|n>0| B("print('Positif')");
    A -->|n <= 0 | C(.);
    C -->|n = 0 | D("print('Nul')");
    C -->|n != 0 | E("print('Négatif')");
    B --> F("print('Fin')");
    D --> F("print('Fin')");
    E --> F("print('Fin')");
    F --> ...
    ```

## 3. Expressions booléennes

Le choix d'une branche ou d'une autre dépend du résultat d'une expression booléenne qui vaut donc
*vrai* (en python :`True`) ou *faux* (en python :`False`).

Pour rappel : les symboles de comparaison (ou d'appartenance) permettant d'écrire une condition
 sont précisées dans le chapitre "Valeurs et expression booléennes".



*Références* :

 - T.Balabonski, S.Conchon, JC.Filliâtre, K.Nguyen  *Numérique et Sciences Informatiques*, Ed. Ellipses  
 - G.Lassus (lycée François Mauriac, Bordeaux), [Première NSI - Base de Python, Instruction conditionnelle if](https://glassus.github.io/premiere_nsi/T1_Demarrer_en_Python/1.4_Instruction_conditionnelle_if/cours/)  
 - G.Becker, S.Point (lycée Mounier, Angers), [Algorithmique et Python, les bases](https://info-mounier.fr/premiere_nsi/bases)