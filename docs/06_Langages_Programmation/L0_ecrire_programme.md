---
author: Bruno Bourgine
title: Écrire un programme
hide:
  - footer
---

!!! note ""

    Un programme est une suite d'instructions données à un **ordinateur** afin qu'il effectue *
    une tâche déterminée.

Cependant, un ordinateur n'est qu'une machine électrique qui ne peut comprendre directement que des suites d'instructions exprimées en langage machine par des suites de 0 et de 1. Le rôle d'un langage de programmation est de fournir au programmeur une **syntaxe** et une **grammaire** pour lui permettre d'exprimer des instructions de manière plus facilement compréhensible pour un être humain. Ce programme est ensuite traduit en langage machine, puis exécuté.

Cette traduction est faite, suivant les langages, par un **compilateur** (programme qui crée un fichier exécutable à partir du code source) ou par un **interpréteur** (programme qui lit, traduit et exécute immédiatement une à une les instructions du code source).

Il existe de nombreux langages de programmation et, même en utilisant un même langage, un algorithme peut être programmé de différentes façons. Un algorithme donné peut donc correspondre à de très nombreux programmes informatiques différents.


## 1. Programmation impérative

### 1.1 Un paradigme ?  

!!! info "Définition"

    > "La programmation impérative est un ***paradigme*** de programmation qui
    décrit les opérations en **séquences d'instructions** exécutées par
    l'ordinateur pour **modifier l'état du programme**."

    sachant que :

    > "Le ***paradigme*** de programmation est la façon (parmi d'autres) d'approcher la programmation informatique et de formuler les solutions aux problèmes et leur formalisation dans un langage de programmation approprié."

     (Wikipédia)

!!! example "Exemples de langages impératifs"

    - Langage machine / Assembleur  (comme Y86)
    - Fortran  
    - C  
    - Pascal  
    - JavaScript  
    - Python

    entre autres sont des langages impératifs.

#### Autres paradigmes

Il existe d'autres paradigmes de programmation : 

- la programmation **orientée objet** (Java, C++, Ada, PHP, ...), 
- la programmation **déclarative** (Caml, Haskell, HTML, LaTeX, ...) , 
- la programmation **fonctionnelle** (OCaml, ...)
    
Python permet également de travailler en programmation orientée objet, et 
en programmation fonctionnelle.

Pour en savoir plus, il faut garder NSI en Terminale.

### 1.2 Description détaillée

L'objectif d'un programme informatique est de "calculer" une solution à un problème. 

| Problème | | |  | Solution |
|:---:|:---:|:---:|:---:|:---:|
|ensemble d’informations données en entrée du problème : **état initial** | ⇒  | États intermédiaires | ⇒ | ensemble d'informations calculées fournies en sortie du programme : **état final**|


L'état d'un programme à un instant `t` est constitué d'un ensemble de
**variables** dont chacune contient une partie d'information. En
programmation impérative, on passe d'un état à un autre par une
**instruction**.

### 1.3 Exemple de programme

!!! example "Exemple introductif : Conversion Euro/Dollar"

    **Instructions désirées** : calcul de la valeur en dollar et affichage de la valeur calculée.

    **Variables à utiliser** : valeur à convertir, taux de conversion et résultat de la conversion.

    === "Algorithme en Pseudo-Code"

        ```
        Déclarations
            tauxConversion, valeurEnEuro, resultat : Entier
        Début
            tauxConversion ← 1,14  
            valeurEnEuro ← 1000
            resultat ← valeurEnEuro × tauxConversion 
            Écrire(resultat) 
        Fin
        ```          

    === "Algorithme en Python :snake:"
        ```python
        # Déclarations (non indispensables et sans conséquence sur l'exécution du programme)
        tauxConversion : float
        valeurEnEuro : float
        resultat : float
        # Début
        tauxConversion = 1.14
        valeurEnEuro = 1000
        resultat = valeurEnEuro * tauxConversion
        print(resultat)
        # Fin
        ```

Python est un **langage de programmation**, il s'agit donc d'un ensemble d'**instructions** 
qui va nous permettre d'écrire des **programmes** informatiques. 

Le langage support de l'enseignement de la NSI au lycée étant Python, ce cours a pour objectif
de vous faire découvrir l'histoire de ce langage, de vous proposer quelques outils de programmation et de vous donner les bases afin de bien démarrer.

## 2. Histoire de Python

Le langage Python a été publié en 1991 par [Guido Van Rossum](https://fr.m.wikipedia.org/wiki/Guido_van_Rossum). 
Il a été nommé ainsi en l'honneur de la troupe de comédiens britanniques des
[Monty Python](https://fr.wikipedia.org/wiki/Monty_Python). L'accessibilité de
ce langage, le fait qu'il soit disponible sous licence libre et qu'il soit muliplateforme
en font un support idéal d'apprentissage de la programmation.

Le langage Python est utilisé pour de nombreuses application, depuis les objets connectés, 
à la gestion de sites web, ou à la sciences des données.


## 3. Travailler avec Python

### 3.1 Installation du langage

Pour travailler avec Python, il faut d'abord avoir [téléchargé](https://www.python.org/downloads/)
et installé le langage correspondant à son système d'exploitation.

### 3.2 Utilisation de la console

La console Python est une ligne de commande interactive dans laquelle on peut
écrire, exécuter une instruction et obtenir le cas échéant le résultat de cette 
commande.

!!! example "Usage de la console depuis un terminal"

    === "avec Linux"

        ```sh
        $ python
        >>> 4 + 1
        5
        ```

    === "avec Windows"

        ```sh
        C:\> py 
        >>> 4 + 1
        5
        ```

### 3.3 Ecriture d'un script

Lorsque l'on souhaite écrire et sauvegarder un programme (ou script), on utilise un éditeur
de texte. On peut écrire en Python avec n'importe quel éditeur de texte, néamoins l'utilisation
d'éditeurs conçus pour la programmation facilite grandement le travail grâce à la coloration syntaxique,
l'indentation automatique ou encore l'autocomplétion.

Quelques exemples d'éditeurs sont donnés plus bas.

### 3.4 Execution d'un script

Pour exécuter un script depuis la ligne de commande, il suffit de se placer dans le répertoire
du script (ou d'écrire le chemin vers le répertoire) et d'appeler la commande qui lance Python,
 suivie du nom du fichier.


!!! example "Exécution d'un script depuis terminal"

    Supposons que le fichier *mon_script.py* soit enregistré dans le dossier `mon_dossier`
    de votre répertoire `utilisateur` et contiennent :

    ```python
    print("Bonjour les gens")
    ```

    === "Exécution avec Linux"

        ```sh
        $ /utilisateur/mon_dossier python mon_script.py
        Bonjour les gens
        ```

    === "Exécution avec Windows"

        ```sh
        C:\utilisateur\mon_dossier > py mon_script.py
        Bonjour les gens
        ```


### 3.5 Utilisation d'un IDE

**Logiciels**

Lorsque l'on programme, on choisit un [environnement de développement](https://fr.wikipedia.org/wiki/Environnement_de_d%C3%A9veloppement) (appelé aussi IDE) approprié.
L'intérêt d'un tel outil est qu'il permet de faciliter l'écriture de code mais aussi de l'exécuter
et de le débuguer si besoin.

[Thonny](https://thonny.org/) est par exemple un IDE conçu pour Python disponible sous licence libre, simple mais 
suffisamment complet pour l'apprentissage du langage. Dans les lycée on retrouve aussi très souvent
[Edupython](https://edupython.tuxfamily.org/).

Parmis les IDE plus élaborés et plus complets permettant de travailler avec différents langages on peut
citer le logiciel libre [VSCodium](https://vscodium.com/).

**Outil en ligne**

On trouve aussi des éditeurs en lignes, comme par exemple la console sur le site
[basthon.fr](https://console.basthon.fr/) qui propose en vis-à-vis un éditeur et une console.

![console basthon](https://cgouygou.github.io/1NSI/images/basthon.png)

**Inclus dans le cours**

Dans les pages de ce cours vous pourrez parfois retrouver une console intégrée. Vous pouvez y 
entrer des instructions simples:

{{ terminal() }}

Vous pourrez parfois aussi rencontrer un IDE intégré à la page avec éventuellement du 
code déjà présent qu'il est possible d'exécuter, modifier, sauvegarder, importer ...

{{ IDEv('mini') }}

## 4. Programmer avec Python

!!! warning "Objectifs"

    L'objectif ici est de vous présenter les commandes de bases qui vont vous permettre
    de commencer à programmer. Cela doit éventuellement permettre de vous remémorer 
    des notions rencontrées en secondes. Nous reviendrons plus en détail sur les éléments
    tels que les variables, les conditions, les boucles et les fonctions dans d'autres 
    parties du cours.

### 4.1 Opérateurs

- `+`, `-`, `*` : addition, soustraction, multiplication.  
- `/` : division.  
- `//` et `%` : quotient et reste de la division euclidienne.  
- `**` : puissance : `a**b` vaut a<sup>b</sup>  

**Priorité des opérateurs**

Les opérateurs multiplicatifs `*`, `/` et `%` ont une priorité supérieure à celle des
opérateurs additifs `+` et` -`.

**Opérateurs de comparaison**

- `==` ,  `!=` : égal, non égal.   
- `<`, `<=`,` >`, `>=` : inférieur, inférieur ou égal, supérieur, supérieur ou égal.


### 4.2 Variables

Un identificateur est une suite de lettres et chiffres qui commence par une lettre et n’est pas
un mot réservé(comme `if`, `else`, `def`, `return`, etc.). Le caractère `_` est considéré comme 
une lettre.

Exemples : `prix`, `x`, `x2`,`nombre_de_pieces`, `vitesseDuVent`, etc.

Majuscules et  minuscules n’y sont pas équivalentes : `prix`, `PRIX` et `Prix` sont trois 
identificateurs distincts.

Une variable est constituée par l’association d’un identificateur à une valeur. Cette association 
est créée lors d’une affectation, qui prend la forme :

`variable = valeur`

On dit que *l’identificateur étiquette la valeur* car il permet de la retrouver en mémoire.

### 4.3 Types

La donnée représentée par une valeur a toujours un type qui détermine ses propriétés formelles 
(comme : quelles opérations la donnée peut-elle subir ?) et matérielles (comme : combien d’octets 
la donnée occupe-t-elle dans la mémoire de l’ordinateur ?).

Les premiers types que nous rencontrerons en Python sont

- les entiers (`int`);  
- nombre décimal (appelé aussi flottant, `float`);  
- chaîne de caractère (suite de caractères quelconques, `string`).
- booléen (`bool`), variable expression qui s'évalue à vrai (`True`) ou faux (`False`.)

Une chaı̂ne de caractères  s’indique en écrivant les caractères en question soit entre 
apostrophes, soit entre guillemets droits : `’Bonjour’` et `"Bonjour"` sont deux écritures 
correctes de la même chaı̂ne.

### 4.4 Boucles


Souvent, dans un programme, il est nécessaire de répéter un certain nombre de fois 
une (ou des) instruction(s). Pour cela, on utilise ce qu'on appelle des boucles. 
Il en existe de deux sortes :

- les boucles bornées, qui sont utilisées si on connaît à l'avance le nombre de répétitions 
  à effectuer ;  
- les boucles non bornées, qui sont utilisées si on ne connaît pas à l'avance le nombre de 
  répétitions à effectuer.

!!! note "Boucle bornée"

    Pour écrire une boucle bornée en Python on utilise le mot-clé `for`. 

    Le mot-clé `for` peut s'utiliser de deux manières :

    - avec la fonction `range()` : pour itérer sur une liste d'entiers;  
    - seulement avec le mot-clé `in` : pour itérer sur les éléments de certaines "variables"

    mais nous nous concentrerons dans cette séance uniquement sur la première possibilité 
    avec `range()`

    La syntaxe la plus simple d'une boucle for utilisant la fonction range() est la suivante.

    ```python
    for indice in range(n):
        bloc_instructions
    ```

    On peut aussi utiliser de la fonction range() pour générer des entiers entre deux 
    bornes, cela donne

    ```python
    for indice in range(debut, fin):
        bloc_instructions
    ```

!!! note "Boucle non-bornée"

    Pour écrire une boucle bornée en Python on utilise le mot-clé `while`. 

    Le bloc d'instructions contenu dans cette boucle sera exécuté tant que la condition
    préalable sera vraie. La condition est donc une expression booléenne (qui s'évalue à 
    vrai ou faux).

    ```python
    while condition :
        bloc_instructions
    ```

### 4.5 Conditions

!!! info ""

    Une instruction conditionnelle, ou instruction de test, permet de faire des choix en 
    fonction de la valeur d’une condition. On parle souvent d’une instruction *si-alors*, 
    ou *if-else* en anglais.

Pour écrire des instructions conditionnelles en Python, il faut utiliser la syntaxe suivante :

```python
if condition1:
	bloc_instructions_1
elif condition2:
	bloc_instructions_2
else:
	bloc_instructions_3
```

Les mots-clés `if`, `elif` (contraction de else if) et `else` sont les traductions respectives 
de “si”, “sinon si” et “sinon”.


### 4.6 Entrée/sortie

Il arrive que l'on souhaite que notre programme interagisse avec l'utilisateur, soit en lui affichant 
une valeur, soit en lui demandant de saisir une valeur.

Pour afficher une chaine de caractère :

```python
print("message à afficher")
```

Pour afficher le contenu d'une variable :

```python
print(ma_variable)
```

Pour demander la saisie d'une valeur et l'enregistrer dans une variable :

```python
reponse = input("la question qui va s'afficher")
```

### 4.7 Appel de fonctions

!!! info ""

    Une fonction est un ensemble d'instructions qui peut recevoir des paramètres (qui sont des 
    valeurs ou des variables) et qui peut renvoyer le contenu d'une ou plusieurs variables.

    Une fonction permet ainsi d'encapsuler une suite d'instruction, que l'on va alors utiliser à 
    loisir avec différents paramètres. On dit qu'on **appelle une fonction**.

    ```python
    ma_fonction(paramètre)
    ```

Remarque : les instructions `print()` et `input()` vues précédemment sont des fonctions.


### 4. Exercices

!!! example "Table de multiplication"

    Écrire dans l'IDE ci-dessous un programme qui demande la saisie d'un nombre 
    entre 1 et 12 et qui affiche sa table de multiplication (de 0 à 10 inclus).

    {{ IDEv () }}

!!! example "Diviseurs"

    Écrire dans l'IDE ci-dessous un programme qui demande la saisie d'un nombre 
    entre 0 et 100 et qui affiche ses diviseurs.

    {{ IDEv () }}


??? note "Sources"

    - *Mise en œuvre informatique, Introduction à la programmation en Python* , Benoit Favre, Université d’Aix-Marseille. Faculté des Sciences. Département d’Informatique.
    - [https://info-mounier.fr/snt/python/](https://info-mounier.fr/snt/python/)