---
author :
title : Fonctions
hide : 
 - footer
---


## 1. Quelques problèmes pour commencer

!!! question "Dessiner un polygone régulier"


    === "Énoncé"

        Supposons que l'on dispose des commandes suivantes en pseudo-codes pour faire des tracés :


        - $Tracer(l)$ : pour tracer un trait de longueur $l$.
        - $Tourner(a)$ :  pour modifier la direction du tracé selon une rotation d'angle $a$.

        1. Écrire en pseudo-code un programme permettant de tracer les polygones réguliers 
        suivant avec un côté de longueur 100 :

        - triangle équilatéral
        - carré
        - pentagone
        - dodécagone

        2. Identifier les instructions qui sont communes à ces algorithmes.

    === "Correction"

        1.
        - triangle équilatéral

        ```
        Début
            pour i ← 1 à 3 faire
                Tracer(100)
                Tourner(120)
            fin pour
        Fin
        ```

        - carré

        ```
        Début
            pour i ← 1 à 4 faire
                Tracer(100)
                Tourner(90)
            fin pour
        Fin
        ```

        - pentagone

        ```
        Début
            pour i ← 1 à 5 faire
                Tracer(100)
                Tourner(72)
            fin pour
        Fin
        ```

        - dodécagone

        ```
        Début
            pour i ← 1 à 12 faire
                Tracer(100)
                Tourner(30)
            fin pour
        Fin
        ```
        2. Pour chacune de ces figure nous avons utilisé une boucle, une instruction `Tracer` et une instruction `Tourner`.

!!! question "Établir une facture"

    === "Énoncé"

        On souhaite établir une facture à partir des achats suivants :

        | Produit | Prix hors taxe | Quantité | Remise % |    TVA %  |
        | :-----: | :------------: | :------: |:-------: |:---------:|
        |    A    |      10,6      |    3     |    2     |    20     |
        |    B    |      8,5       |    10    |    0     |    5,5    |
        |    C    |      13,2      |    5     |    7     |    20     |

        1. Écrire en pseudo-code un programme permettant de calculer le prix TTC de l'ensemble 
        de ces produits.

        2. Identifier les groupes  d'instructions que l'on retrouve à plusieurs reprise dans ce programme.

    === "Correction"

        1. 
        ```
        Déclarations
            factureA, factureB, factureC, total : entiers
        Début
            factureA ← 10,6 × 3
            factureA ← factureA × (1-0,02)/100
            factureA ← factureA × (1+0,2)/100
            factureB ← 8,5 × 10
            factureB ← factureB × (1+0,055)/100
            factureC ← 13,2 × 5
            factureC ← factureC × (1-0,05)/100
            factureC ← factureC × (1+20)/100
            facture ← factureA + factureB + factureC
        Fin
        ```

        2. Pour chaque produit on calcule le prix HT total, puis on utilise ce résultat auquel on soustrait une remise puis on utilise ce nouveau résultat pour lui ajouter la TVA.


### Bilan

![repeated_code](res/repeatedcode.jpeg){width=50%}

Dans les exemples vus précédemment nous retrouvons des fragments de codes très similaires
qui sont répétés de multiples fois. Nous allons voir comment améliorer la lisibilité 
et l'exécution de notre code en utilisant des fonctions qui permettrons d'éviter de nous 
répéter.



## 2. Définir une fonction

En informatique, une fonction est une entité qui encapsule une portion de code (une séquence 
d’instructions) effectuant un traitement spécifique bien identifié et qui peut être réutilisé 
dans le même programme (ou dans un autre).

Concrètement on va associer un nom à une séquence d'instructions que l'on va pouvoir utiliser 
en appelant le nom choisi.

### 2.1. Premier exemple

!!! example "Tracer un triangle équilatéral"

    Voici une fonction écrite en python (avec turtle) qui permet de tracer un triangle équilatéral.
    On utilise cette fonction dans une boucle.

    {{ IDEv ('scripts/triangle')}}


!!! warning "Syntaxe d'une fonction en Python"

    La définition d'une fonction commence par le mot clé `def`, suivi du nom de la fonction comme 
    `triangle_equilateral`  puis de parenthèses `()` et enfin deux points `:`.

    Les instructions contenues dans la fonction s'appelle le *corps* de la fonction. Tout comme
    pour les boucles ou les tests, le corps de la fonction est *indenté* par rapport au mot clé `def`.

    Pour exécuter les instructions de la fonction, il suffit alors d'écrire son nom suivi des 
    parenthèses comme dans l'exemple précédent : `triangle_equilateral()`. On dit qu'on *appelle* 
    la fonction.

### 2.2. Fonction avec un paramètre

La fonction `triangle_equilateral` fait bien ce qu'on lui demande, mais elle est tout de même limitée.

Il serait ainsi souhaitable de pouvoir choisir la longueur du côté, pour réaliser cela nous allons y ajouter un *paramètre d'entrée*.


!!! example "Paramètre longueur"

    Nous allons ajouter un paramètre nommé "longueur", que nous allons ensuite utiliser
    dans le corps de la fonction en lieu et place de la valeur fixée à 50 dans la fonction
    initiale.

    ```python
    def triangle_equilateral(longueur):
      for _ in range(3):
        forward(longueur)
        left(120)
    ```
    On voit ici qu'on a déclaré le paramètre "longueur" en le plaçant entre les parenthèses, 
    puis qu'on l'a utilisé dans le corps de la fonction, à la manière d'une variable.

Pour appeler une telle fonction, il faudra maintenant ajouter une valeur entre les parenthèses 
au moment de l'appel, par exemple `triangle_equilateral(100)`. À ce niveau, rien ne nous garantit
que le paramètre passé soit du type attendu par la fonction, nous verrons plus tard comment anticiper
ce problème.


### 2.3. Fonction avec plusieurs paramètres

On peut s'inspirer de la fonction précédente afin de tracer n'importe quel autre polygone régulier.
Pour cela nous aurons besoin d'un paramètre supplémentaire : le nombre de côtés.


!!! example "Paramètre nombre de côtés"

    Nous allons ajouter maintenant un paramètre nommé `cotes`, que nous allons ensuite utiliser
    dans le corps de la fonction pour :  

      - modifier le nombre de répétitions,  
      - modifier la valeur de l'angle.
  
    Cette fonction permettant maintenant de tracer un polygone régulier, nous allons lui donner 
    un nom adéquat.

    {{ IDEv ('scripts/polygone')}}
  
Que ce soit lors de la définition ou lors de l'appel , les paramètres sont placés entre parenthèses
et séparés par des **virgules**. L'ordre des paramètres lors de l'appel doit correspondre à celui
de la définition, à moins de passer les paramètres de façon explicite comme par exemple :

```python
polygone_regulier(cotes = 5, longueur = 60)
```


### 2.4. Fonctions prédéfinies (Built-in functions)

À ce niveau, vous avez du vous rendre compte que la syntaxe d'utilisation des fonctions que l'on 
a définie dans nos exemples correspond à celle d'instructions que nous avons utilisées jusqu'à présent
comme `print`, `input` ou `range`. Celles-ci constituent ce qu'on appelle des fonctions prédéfinies 
(ou built-in).

Vous pouvez consulter la [documentation](https://docs.python.org/fr/3/library/functions.html) pour 
connaître l'ensemble des fonctions prédéfinies en python.


## 3. Renvoyer un résultat


![](res/not-outputting.jpg){width=50%}

!!! warning "Return"

    En mathématiques, une fonction peut être assimilée à un procédé qui prend un nombre 
    (*antécédent*) et en renvoie un autre (*image*).
    
    En python, le mot-clé `return` permet d'indiquer ce qui sera **renvoyé** par la fonction.

!!! example "Exemple : une fonction affine"

    En python la fonction $f:x \rightarrow 2x + 6$ pourra être codée par :

    ```python
    def f(x):
      return 2*x + 6
    ```
    Ici la valeur qui est renvoyé est le résultat de l'expression `2*x + 6`

En informatique, l'objet renvoyé ne sera pas forcément un nombre (cela pourra être aussi une
 chaine de caractères, un tableau, une image, une fonction ...).

L'appel d'une fonction renvoyant un résultat peut aussi être utilisé comme une expression ou comme 
une partie d'expression plus complexe. 

!!! example "Calcul d'une facture"

    Si l'on reprend le problème posé en introduction, nous pouvons le résoudre en  définissant
    les fonctions suivantes :

    ```python
    def calculer_total_ht(prix, quantite):
      return prix * quantite

    def calculer_ttc(ht, taux):
      return ht*(1+taux)/100
    ```
    
    Ainsi pour le produit "A" tel que :

    | Produit | Prix hors taxe | Quantité | taux de TVA |
    | :-----: | :------------: | :------: | :---------: |
    |    A    |      10,6      |    3     |     20      |

    On peut utiliser les fonctions ainsi :

    ```python
    factureA = calculer_ttc(calculer_total_ht(10.6, 3), 20):
    ```

!!! warning "Fonction ou procédure ?"

    Une fonction qui ne renvoie pas de résultat comme la fonction `polygone_regulier`
    de l'exemple plus haut est aussi appelée une *procédure*. Il faut distinguer les
    usages d'une procédure et d'une fonction :

    - une **procédure réalise une action** (affichage, dessin), elle peut par exemple servir
     à remplacer une suite d'instructions à différents endroit du code.  
    - une **fonction a pour objectif de produire et mettre à disposition un résultat** 
    (valeur numérique, chaine de caractères...), qui pourra alors être affecté à une variable 
    ou utilisé dans une expression plus grande.

    En l'absence de `return`, Python renvoie tout de même par défaut une valeur spéciale : 
    `None` qui exprime l'absence de valeur. Dans une expression booléenne, `None` est 
    assimilé à `False`.
    


!!! warning "Sortie anticipée par `return`"

    Le mot-clé `return`ne se contente pas seulement de renvoyer le résultat d'une fonction, 
    il interrompt l'exécution de celle-ci : aucune des instructions qui serait présente dans 
    la suite du code ne serait alors exécutée. Cela permet d'interrompre une fonction lorsque
    le résultat souhaité est atteint.

!!! example "Vérifier si un triangle est rectangle"

    Supposons que nous voulions écrire une fonction permettant de vérifier si un triangle
    est rectangle, à partir des longueurs (entières) de ses côtés données dans un ordre
    quelconque.

    Une façon de l'écrire pourrait être :

    ```python
    def est_rectangle(a,b,c) :
      if a**2 == b**2 + c**2 :
        return True
      if b**2 == a**2 + c**2 :
        return True
      if c**2 == b**2 + a**2 :
        return True
      return False
    ```
    Ainsi si la première condition est vérifiée, les autres tests ne seront pas exécutés.

## 4. Variables locales, variables globales

### 4.1. Espace de nommage d'une fonction

Dans l'exemple de la fonction `polygone_regulier`, on utilise une variable dans la fonction
à laquelle on affecte la valeur de l'`angle`. Ce type de variable est appelée **variable locale**
à la fonction.

```python linenums="1" hl_lines="4"
def polygone_regulier(longueur, cotes):
      for _ in range(cotes):
        forward(longueur)
        angle = int(360 / cotes)
        left(angle)
```

Cette variable est définie à l'intérieur de la fonction au moment de son initialisation et
elle disparait à la fin de l'exécution de la fonction. La variable `angle`n'existe pas à l'extérieur
de la fonction `polygone_regulier`.


On dit que les fonctions créent leur «espace de nommage» (*espace* est à prendre au sens d'*univers*),
 un espace qui leur est propre et dans lequel sont listés les noms des variables locales.

### 4.2. Règles d'accès en lecture et en modification d'une variable suivant son espace d'origine

!!! warning "Règles d'accès aux variables locales et globales"

    - **Règle 1 :** une **variable locale** (définie au cœur d'une fonction) est **inaccessible** hors de cette fonction.  
    - **Règle 2 :** une **variable globale** (définie à l'extérieur d'une fonction) est **accessible** en **lecture** à l'intérieur d'une fonction.  
    - **Règle 3 :** une **variable globale** (définie à l'extérieur d'une fonction) **ne peut pas être modifiée** à l'intérieur d'une fonction.

!!! example "Exercice"
    === "Énoncé"

        On considère les 3 codes ci-dessous. Pour chacun, dire **sans l'exécuter** s'il est valide ou non. S'il ne l'est pas, identifier la règle (parmi celles énoncées ci-dessus) qui est bafouée.

        **code A**
        ```python linenums='1'
        points = 0
        def verdict(reponse):
            if reponse > 10:
                points += 3

        verdict(12)
        ```    

        **code B**
        ```python linenums='1'
        def bouge(x, decalage):
            x += decalage

        bouge(100, 5)
        print(x)
        ```

        **code C**
        ```python linenums='1'
        def test_bac(moyenne):
            if moyenne >= 10:
                print("admis !")

        def coup_de_pouce(note):
            return note + bonus

        bonus = 0.6
        ma_moyenne = 9.5
        ma_moyenne = coup_de_pouce(ma_moyenne)
        test_bac(ma_moyenne)
        ```

    === "Correction code A"
        Ce code n'est pas valide, car il contrevient à la règle 3.

        ```ligne 4``` : la modification de la variable globale ```points``` est interdite.

    === "Correction code B"
        Ce code n'est pas valide, car il contrevient à la règle 1.

        ```ligne 5``` : l'accès à la variable locale ```x``` est interdit.

    === "Correction code C"
        Ce code est valide.

        ```ligne 6``` : l'accès à la variable globale ```bonus``` est autorisé, selon la règle 2.            


!!! danger "À propos de la règle n°3"
    
    Pour certains types de variables (listes, dictionnaires...), la modification d'une variable globale à l'intérieur du corps d'une fonction est en fait possible (contrairement à ce qu'énonce la règle 3). Mais cela reste très fortement déconseillé.

    Pour les autres types de variables,  on peut même *forcer* pour avoir cette possibilité en utilisant le mot ```global``` à l'intérieur de la fonction. 
    
    Mais il faut essayer d'éviter ceci. Une fonction ne **doit** (c'est un ordre, mais vous pouvez choisir de l'ignorer, tout comme vous pouvez choisir de passer au feu rouge) modifier que les variables qu'elle crée (ses variables locales) ou bien les variables qu'on lui a données en paramètre. 

    Une fonction qui ne respecte pas cette règle présente des _effets de bord_ : on peut peut-être arriver à les gérer sur un «petit» code, mais cela devient illusoire sur un code utilisant de multiples fonctions. 

**En résumé**

Ne pas faire cela :

```python linenums='1' hl_lines="4"
# PAS BIEN
score = 0
def ramasse_objet(objet):
    global score
    if objet == "champignon":
        score += 20
    if objet == "banane":
        score -= 300
```
puis :

```python
>>> ramasse_objet("champignon")
>>> score
20
```

Faire plutôt ceci :

```python linenums='1' hl_lines="3 8"
    # BIEN
score = 0
def ramasse_objet(objet, score):  # ma fonction veut modifier score ? 
    if objet == "champignon":     # -> ok, je mets score dans ses paramètres
        score += 20
    if objet == "banane":
        score -= 300
    return score # je renvoie le nouveau score
```

```python
>>> score = ramasse_objet("champignon", score)
>>> score
20
```

## 5. Traduire en pseudo-code

Lors de l'écriture d'une fonction en pseudo-code, on prendra soin de préciser le type des paramètres
d'entrée ainsi que celui de la valeur renvoyée.

```code
fonction <nom>(<parametre_1> : <Type>, ..., <parametre_n> : <Type>) : <TypeRetour>
    <bloc d’instructions>
renvoyer <valeur renvoyée>

fin fonction
```

Si plusieurs paramètres en entrée sont de même type, on peut les regrouper.

!!! example "Exemple : calculer le maximum de deux valeurs"

    === "Pseudo-code"

        ```
        fonction MAX(a, b : Entier) : Entier  
          Si a > b alors  
              renvoyer a    
          Sinon  
              renvoyer b  
          Fin si  
        Fin fonction
        ```

    === "Python"

        ```python
        def max(a,b):
            if a > b :
              return a
            else :
              return b
        ```

## 6. Mettre en oeuvre des bonnes pratiques de programmation

### 6.1. Nommer correctement

Un nom de fonction doit être choisi de façon explicite de façon à traduire correctement 
ce que fait celle-ci. Dans le cas d'un nom composé, l'usage en Python est d'écrire les
mots en minuscule et de les séparer par un underscore, par exemple : `calculer_tva`.

### 6.2. Déclarer les types de variables

Dans la partie consacrée au pseudo-code, on a rappelé l'usage de renseigner le type de 
variable. Certains langages de programmation (C, Java ...) exige de préciser le type des 
paramètres et des renvois lors de la définition de fonctions.

En Python, une telle pratique n'est pas exigée, néanmoins elle est tout de même possible. 
Pour cela on utilise ce qui est désigné par 
 [annotation de type](https://docs.python.org/fr/3/library/typing.html) (ou `type hints`).

!!! warning "Signature d'une fonction"

    La signature d'une fonction correspond aux noms et types de paramètres d'entrée et de 
    sortie d'une fonction.

    Il faut **toujours** veiller à écrire une signature correcte et complète de fonction. 
    En Python, on précise le type d'un paramètre en l'indiquant à la suite de celui-ci, 
    derrière `:`. Pour le type de la valeur renvoyée, on l'écrit avant les `:` terminant 
    la ligne de signature et en le précédent de `->`.

    La syntaxe est :
    ```python
    def nom_de_ma_fonction (param1 : type_de_param1, param2 : type_de_param2) -> type_de_sortie :
        ...
    ```


!!! example "Signature de la fonction `calculer_total_ht` "

    Si l'on reprend cette fonction vue dans la partie 3 :

    ```python
      def calculer_total_ht(prix, quantite):
        return prix * quantite
    ```

    Elle prend comme paramètres `prix` qui est de type `float` et `quantite` qui peut être 
    de type `int`, et elle renvoie une valeur de type `float`.

    En ajoutant les annotations de type, la signature devient :

    ```python
      def calculer_total_ht(prix : float, quantite : int) -> float :
        return prix * quantite
    ```

!!! note "Juste une indication"

    Il faut bien comprendre qu'il s'agit seulement d'une indication permettant la bonne 
    compréhension du code. Si une fonction est appelée avec des paramètres dont le type
    ne correspond pas aux annotations, cela ne bloquera pas l'appel de la fonction.


### 6.3. Documenter

![RTFD](res/doc.png){width=50%}

Il est possible, voire souhaitable (dès qu'on créé un code comportant plusieurs fonctions,
et/ou qui sera amené à être lu par d'autres personnes), de créer un mode d'emploi pour ses 
fonctions. On appelle cela écrire **la docstring** de la fonction, et c'est très simple : 
il suffit de l'encadrer par des triples double-quotes ```"""``` .

!!! example "Exemple"

    ```python
      def polygone_regulier(longueur : float, n : int) -> None:
        """
        Trace un polygone régulier à `n` côtés de longueur `longueur`
        Entrées:
        --------
         * longueur (float) : la longueur du côté du polygone
         * n (int) : le nombre de côtés du polygone
        Sortie :
        --------
         * (None) 
        """

        for _ in range(n):
          forward(longueur)
          angle = int(360 / n)
          left(angle)
    ```

    On peut alors appeler l'aide sur cette fonction :

    ```
    >>> help(polygone_regulier)
    Help on function polygone_regulier in module __main__:

    polygone_regulier(longueur: float, n: int) -> None
    Trace un polygone régulier à `n` côtés de longueur `longueur`
    Entrées:
    --------
     * longueur (float) : la longueur du côté du polygone
     * n (int) : le nombre de côtés du polygone
    Sortie :
    --------
     * (None)
    ```

### 6.4. Tester

Une bonne pratique lorsque l'on écrit une fonction est en premier lieu de définir une 
*fonction de test*. Il s'agit d'une fonction, souvent appelée ```test_nom_de_la fonction()```, 
qui va vérifier que la fonction a le comportement désiré.

Ces tests reposent sur le mot-clé ```assert```, qui va lever une erreur lorsqu'il est suivi 
d'une expression évaluée à ```False``` :

```python
>>> assert 3 > 2
>>> assert 3 > 5
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
AssertionError
>>> assert True
>>> assert False
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
AssertionError
```

!!! note "Exemple d'un jeu de tests"
    ```python linenums='1'
    def maxi(n1, n2):
        if n1 < n2 :
            return n2
        else :
            return n1

    def test_maxi():
        assert maxi(3,4) == 4
        assert maxi(5,2) == 5
        assert maxi(7,7) == 7
        print("tests ok")
    
    ```

## 7. En conclusion

!!! warning ""

    - Une fonction isole un fragment de programme répondant à un objectif et lui associe un nom. On
    utilise ce nom pour appeler la fonction et ainsi exécuter les instructions qu'elle contient.  
    - L'objectif d'une fonction peut être la réalisation d'une action ou le calcul d'une valeur.  
    - L'utilisation de fonctions permet de décomposer un problème en sous-problème.  
    - Une fonction doit comporter une signature complète comprenant les indications de type.

    Dorénavant, il vous sera demandée d'encapsuler votre code dans des fonctions, a fortiori
    lorsque vous devrez calculer un résultat à partir de paramètres. Autrement dit : on "oublie" les `input` et les `print` -> on utilise des paramètres et on renvoie des résultats.

![bye](res/bye.gif)


références :

 - T.Balabonski, S.Conchon, JC.Filliâtre, K.Nguyen  *Numérique et Sciences Informatiques*, Ed. Ellipses
 - C.Adobet, G.Connan, G.Rozsavolgyi, L.Signac *Numérique et sciences informatiques SPÉCIALITÉ*, Ed. Hatier
 - G.Lassus (lycée François Mauriac, Bordeaux), [Première NSI - Base de Python, Instruction conditionnelle if](https://glassus.github.io/premiere_nsi/T1_Demarrer_en_Python/1.4_Instruction_conditionnelle_if/cours/)
 - G.Becker, S.Point (lycée Mounier, Angers), [Algorithmique et Python, les bases](https://info-mounier.fr/premiere_nsi/bases)
 