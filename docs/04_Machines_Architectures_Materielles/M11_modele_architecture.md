---
author :
title : Modèle d'architecture séquentielle
hide : 
 - footer
---

# Les constituants d’un ordinateur

## 1 Du plus petit au plus grand

### 1.1 Le transistor

!!! exemple inline end ""

    ![transistor](res/Transistor.png)

!!! note ""

    Le plus petit composant que nous abordons dans ce cours est le transistor (image ci-contre). 
    Il s’agit d’un composant électronique qui se comporte comme un interrupteur commandé : soit il 
    laisse passer le courant (interrupteur fermé ou état “haut” représenté par la valeur “1”), 
    soit il ne laisse pas passer le courant (interrupteur ouvert ou état “bas” représenté par la
    valeur “0”).

    Depuis longtemps déjà, au sein des ordinateurs, on ne trouve plus de “gros” transistors isolés 
    comme celui de la figure. Ils sont aujourd’hui gravés sur des plaques de silicium, tout comme 
    les connexions entre ces transistors.

### 1.2 Les circuits logiques

Lorsque l’on combine plusieurs transistors, on peut former des portes logiques. Il existe
7 portes logiques différentes :

- NON (NOT)  
- OU (OR)  
- ET (AND)   
- OU EXCLUSIF (XOR)   
- NON OU (NOR)  
- NON ET (NAND)  
- NON OU EXCLUSIF (NXOR)  


À chaque porte logique est associée une table de vérité, permettant de connaı̂tre les valeurs
binaires en sortie de ces portes en fonction des valeurs en entrée (voir Fiche correspondante sur
les circuits logiques).

!!! exemple inline end ""

    <iframe src="https://circuitverse.org/simulator/embed/21530"></iframe>

    <!-- ![additionneur](res/Additionneur.png) -->


!!! note "Circuits logiques"

    En combinant les portes logiques, on obtient des circuits logiques. Par exemple, 
    avec 2 portes XOR, 2 portes ET et 1 porte OU combinées convenablement, on peut former 
    un additionneur (figure ci-contre).


### Exercices

??? question "Exercice 1 : porte NAND"

    Une porte NAND permet de réaliser l'opération logique `nand` (`not-and` ou `non-et` en français). 
    Cet opérateur est défini de la manière suivante :

                a nand b = not (a and b)

    Construire avec le simulateur ci-dessous une porte NAND (ou NON-ET) en utilisant une porte 
    NON et une porte ET. Le circuit est à recopier sur votre cahier.

    <iframe style="width: 100%; height: 250px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=in,not,out,and&data=N4NwXAbANA9gDgFwM5mAXzUA"></iframe>


??? question "Exercice 2 : loi de De Morgan"

    La loi de De Morgan dit que pour tous booléens `a` et `b` :

    `non (a et b) = (non a) ou (non b)`.

    1. Construire le circuit correspondant à l'expression booléenne (non a) ou (non b) 
    (celle de droite dans l'égalité). Le circuit est à recopier sur votre cahier.

    2. Vérifier, en utilisant le simulateur, que ce circuit est équivalent au précédent.

    <iframe style="width: 100%; height: 250px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=in,not,out,or&data=N4NwXAbANA9gDgFwM5mAXzUA"></iframe>


??? question "Exercice 3 : Porte XOR avec ET, OU et NON"

    Construire le circuit correspondant à l'expression `(a et non b) ou (non a et b)`. 
    Le circuit est à recopier sur votre cahier.

    Vérifier, en utilisant le simulateur, que ce circuit est équivalent à une porte XOR 
    (ou exclusif).

    <iframe style="width: 100%; height: 300px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=in,not,out,or,and,xor&data=N4NwXAbANA9gDgFwM5mAXzUA"></iframe>



??? question "Exercice 4 : comparateur"

    Dans cet exercice on considère le circuit suivant, appelé comparateur, qui permet de 
    comparer les mots binaires `(a, b)` et `(c, d)`.
    
    Le bit de sortie :

    - vaut 1 si `(a, b) = (c, d)` ;
    - vaut 0 sinon.

    ![comparateur](res/comparateur.svg)


    Construire ce circuit avec le simulateur et vérifier qu'il permet bien de comparer 
    deux mots binaires de longueur 2.

    <iframe style="width: 100%; height: 350px; border: 0" src="https://logic.modulo-info.ch/?mode=design&showonly=in,not,out,xnor,and,xor&data=N4NwXAbANA9gDgFwM5mAXzUA"></iframe>



### 1.3 La mémoire

La mémoire permet de stocker des données et des programmes, c’est-à-dire des suites de bits qui seront
des états électriques (haut ou bas).

#### 1.3.1 La mémoire RAM (Random Access Memory)

![RAM](res/RAM.jpg)

La **mémoire RAM**, appelée aussi “mémoire vive”, est un ensemble de cellules (comme pour un tableur),
chacune étant capable de stocker une suite de 8 bits (un octet). Lorsque l’on parle de mémoire RAM 16Go, 
cela signifie que cette mémoire contient 16 milliards de cellules. Chacune des cellules d’une mémoire
RAM possède une adresse (un identifiant).

Sur une mémoire RAM, il est possible soit de lire, soit d’écrire des informations. On dit que
la mémoire RAM est accessible en **lecture** et en **écriture**. Une opération de lecture consiste à 
lire l’octet situé à l’adresse X et une opération d’écriture consiste à écrire un octet à l’adresse Y 
(X et Y étant codés en binaire).

Le stockage d’un bit d’une cellule se fait grâce à un **condensateur**. Il s’agit d’un composant
électronique capable d’être chargé (on stocke un “1”) ou déchargé (on stocke un “0”). Le problème 
principal des condensateurs est qu’ils ne sont pas capables de conserver leur charge sans être 
alimentés électriquement. Le stockage est donc perdu en cas d’extinction de l’ordinateur
ou de coupure de courant. On dit que la mémoire RAM est **volatile**.

#### 1.3.2 Les mémoires de masse (Disque dur, CD, DVD, USB)

Pour conserver les données dans la durée après extinction d’un ordinateur, on utilise des mémoires 
de masse, comme les disques durs, les clés USB, les CD ou les DVD, qui n’ont pas besoin d’alimentation électrique pour conserver les données (utilisation du magnétisme, de l’optique, etc.). La mémoire de 
masse la plus utilisée aujourd’hui est le disque dur. Il en existe 2 formes : les HDD (Hard Disk Drive) 
et les SSD (Solid State Drive).

#### 1.3.3 La mémoire ROM (Read-Only Memory)

Il existe un autre type de mémoire dans un ordinateur, la mémoire ROM, appelée aussi
“mémoire morte”, qui comme son nom l’indique n’est accessible qu’en lecture. La ROM n’est
pas volatile et sert à stocker les informations nécessaires au démarrage de l’ordinateur (le BIOS).

### 1.4 Le microprocesseur (CPU – Central Processing Unit)

Le microprocesseur (voir figure ci-contre) est le “cœur” de l’ordinateur. C’est lui qui gère
le fonctionnement de l’ordinateur.

!!! example inline end ""

    ![microprocesseur](res/Microprocesseur.png)

!!! note "Répartition des rôles au sein d'un microprocesseur"

    Un microprocesseur est constitué de trois partie :

    - L’unité arithmétique et logique (UAL ou ALU en anglais) est chargée de l’exécution de tous les calculs que
    peut réaliser le microprocesseur. C’est dans l’UAL que se trouvent les circuits logiques permettant d’effectuer
    les calculs de l’ordinateur (comme l’additionneur par exemple, voir section 1.2).  
    - Les registres permettent de mémoriser l’information obtenue via la mémoire (souvent la RAM) afin d’effectuer 
     des traitements dessus. Un exemple de traitement est de faire passer un octet dans un circuit logique de 
     l’UAL. Le nombre et la taille des registres sont variables en fonction du type de microprocesseur. 
     Conventionnellement, pour une meilleure compréhension, on nomme ces registres R0, R1, R2, R3, etc.
    - L’unité de commande permet d’exécuter les instructions (donc les programmes). Voir section 2.

### 1.5 Les bus

!!! example inline end ""

    ![bus](res/Bus.png)

!!! note ""

    L’information a besoin de circuler entre les composants (notamment entre la mémoire vive et le CPU). 
    Ce sont les bus qui permettent cette circulation. Il en existe 3 types :

    - Le bus d’adresse permet de faire circuler des adresses (par exemple l’adresse d’une donnée à aller chercher 
    en mémoire).
    - Le bus de données permet de faire circuler des données.
    - Le bus de contrôle permet de spécifier le type d’action (exemples : écriture d’une donnée en mémoire, lecture 
    d’une donnée en mémoire).

## 2. Architecture de von Neumann

!!! note inline end ""

    ![VonNeumann](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/JohnvonNeumann-LosAlamos.jpg/520px-JohnvonNeumann-LosAlamos.jpg){width=50%}

!!! note "Principe"

    La particularité de toutes les architectures des ordinateurs modernes, inventée en 1945
    par le mathématicien et physicien américano-hongrois John von Neumann (ci-contre), est d’utiliser
    la mémoire vive pour stocker à la fois les données et les instructions machine.

    Auparavant, les données et les instructions étaient séparées, ce qui ralentissait considérablement
    les calculs et prenait deux fois plus de place. Lorsque l’on évoque ici les données, il s’agit des
    opérandes pour laquelle la valeur est une adresse de mémoire vive. 


!!! note "Description"

     ![schemaVonNeumann](res/vonNeumann.png){width=50%}

    L’architecture de von Neumann peut être représentée par le schéma ci-dessus pour lequel :

    - La mémoire correspond à la RAM.
    - L’unité arithmétique et logique contient l’accumulateur qui est un registre  permettant de stocker
      les résultats intermédiaires lors d’un calcul.
    - L’unité de commande est aussi parfois appelée “unité de contrôle”.
    - Le système “entrée-sortie” permet de communiquer avec l’extérieur du système CPU/RAM
    (clavier, souris, écran, carte graphique, disque dur. . .).


Pendant des années, pour augmenter les performances des ordinateurs respectant l’architecture
de von Neumann, les constructeurs augmentaient la fréquence d’horloge des microprocesseurs,
c’est-à-dire sa capacité d’exécuter un nombre important d’instructions machines par seconde.

Plus la fréquence d’horloge du CPU est élevée, plus ce CPU est capable d’exécuter un grand
nombre d’instructions machines par seconde. L’évolution de la fréquence d’horloge au cours du
temps est représentée sur le graphique ci-dessous.

![freqCpu](res/FrequenceCPU.png){width=50%}

On peut y voir que la fréquence d’horloge a cessé d’augmenter à partir de 2006. Ceci est dû au fait
que plus on augmente la fréquence d’horloge d’un CPU, plus ce dernier chauffe !

Il devenait difficile de refroidir le CPU, et les constructeurs ont trouvé une autre méthode pour 
augmenter les performances : augmenter le nombre d’UAL, de registres et d’unité de commande d’un 
seul microprocesseur; l’architecture multi-cœurs était née !

La technologie permettant de graver toujours plus de transistors sur une surface donnée, il
est donc possible d’avoir plusieurs cœurs sur une même puce, alors qu’auparavant on trouvait
un seul cœur dans un CPU. En 2019, on trouvait sur le marché des CPU possédant jusqu’à 18
cœurs, et sur les smartphones des CPU possédant 8 cœurs.

Cette évolution technologique possède deux grandes limites actuellement :

- La conception d’applications capables de tirer profit d’un CPU multi-cœur demande
la mise en place de certaines techniques de programmation différentes des techniques
actuelles. Il faudrait donc idéalement reprogrammer entièrement toutes les applications
utilisées aujourd’hui dans le monde pour que le bénéfice soit visible.  
- Les différents cœurs d’un CPU doivent tout de même se “partager” l’accès à la mémoire
vive : quand un cœur travaille sur une certaine zone de la RAM, cette même zone n’est
pas accessible aux autres cœurs, ce qui, bien évidemment va brider les performances. Il
existe cependant une autre mémoire, la mémoire cache, qui permet un accès beaucoup
plus rapide qu’en RAM. Sauf que comme tout composant plus performant, la mise en
place de mémoire cache est extrêmement chère, ce qui fait que l’on en place généralement
qu’une seule par CPU. Le problème d’accès concurrent n’est donc pas entièrement réglé.

## 3. Pour faire fonctionner tout ça : un nouveau langage

Au sein du processeur, l’unité de commande permet d’exécuter des instructions. Ces instructions 
proviennent de programmes. Ces programmes sont des suites d’instructions écrites, par exemple, 
en Python.

L’unité de commande (et par extension le CPU) est incapable de d’exécuter directement des instructions
écrites en Python, car elle ne sait lire que des bits ou des octets. Il est donc nécessaire d’avoir 
un langage de traduction d’instructions Python en instructions exécutables directement par le CPU. 
Ce type de langage est appelé langage machine.

### 3.1 Les instructions machines

#### 3.1.1 La composition d’une instruction

Une instruction de langage machine est une suite binaire composée principalement de 2 parties :

- Le champ code opération qui indique au processeur le type de traitement à réaliser.
Par exemple, le code “00100110” donne l’ordre au CPU d’effectuer une multiplication.  
- Le champ opérandes indique la nature des données sur lesquelles l’opération désignée par le 
“code opération” doit être effectuée.

#### 3.1.2 Les opérandes

Les opérandes sont de 3 sortes :

- Une valeur immédiate. L’opération est alors effectuée directement sur cette valeur.  
- Un registre du CPU. L’opération est effectuée sur la valeur située dans ce registre.  
- Une adresse de mémoire vive. L’opération est effectuée sur la valeur située à cette adresse.  


#### 3.1.3 Les différents types d’instruction

Les instructions possibles au travers du code opération sont de 3 sortes :

!!! note "Instructions arithmétiques"

    Les instructions arithmétiques (addition, soustraction, multiplication. . .) qui effectuent
    des opérations binaires.
    
    Par exemple, on peut avoir une instruction qui ressemble à :

    ```
    additionne la valeur contenue dans le registre R1 et le nombre 789 et range le résultat
    dans le registre R0
    ```
    **Remarque** : les données sont écrites ici en base 10 pour souci de simplicité, n’oubliez pas 
    qu’en interne elles sont codées en binaire.

!!! note "Instruction de transfert"

    Les instructions de transfert de données qui permettent de transférer une donnée d’un registre 
    du CPU vers la mémoire vive et vice versa.
    
    Par exemple, on peut avoir une instruction qui ressemble à :

    ```
    prendre la valeur située à l’adresse mémoire 487 et la placer dans la registre R2
    ```

    ou encore
    ```
    prendre la valeur située dans le registre R1 et la placer à l’adresse mémoire 512
    ```

    **Remarque** : les adresses mémoire sont elles aussi codées en binaire en réalité.

!!! note "Instruction de rupture de séquence"

    Les instructions de rupture de séquence (ou instructions de saut) qui permettent
    de modifier l’exécution “normale” des instructions. Cette exécution “normale” consiste
    à lire les instructions séquentiellement, l’une après l’autre.

    Les instructions de saut permettent d’interrompre cet ordre initial. On en distingue 2 :

    - Les instructions de saut inconditionnelles, qui modifient l’ordre d’exécution dans
    tous les cas.  
    - Les instructions de saut conditionnelles, qui modifient l’ordre d’exécution seulement
    si une condition est respectée.
      Par exemple, nous pouvons avoir une instruction qui ressemble à :

      ```
      si la valeur contenue dans le registre R1 est strictement supérieure à 0 alors 
      exécuter l’instruction n˚4521, sinon exécuter l’instruction n˚355
      ```

### 3.2 L’assembleur

Un programme écrit en langage machine est donc une suite très très longue d’octets.
Pour donner un ordre d’idée, un programme Python d’entre 100 et 1000 lignes correspond
à des dizaines de milliers de bits en langage machine.

Écrire ces suites de bits peut s’avérer extrêmement long et rébarbatif ! Une seule erreur de bit et 
le programme ne fonctionne plus... Et bon courage pour retrouver l’erreur !

Pour pallier cette difficulté, les informaticiens ont remplacé ces octets ou suites d’octets
par des symboles/mots du langage courant. Les instructions machines sont toujours les mêmes,
mais au lieu d’écrire “11100010100000100001000001111101”, on écrira “ADD R1,R2,#125”.

Dans les 2 cas, la signification est identique : “additionne le nombre 125 et la valeur située dans
le registre R2, range le résultat dans le registre R1”.

Par extension, ce sont ces langages écrits dans le langage courant que l’on appelle langages
machines. De nos jours, le langage machine le plus utilisé est le langage assembleur. On dira
alors que l’on programme en assembleur quand on écrit des programmes avec ces symboles/mots
à la place de suites de bits. Aujourd’hui, l’écriture de programmes en assembleur est encore
relativement courante.

#### 3.2.1 Exemples d’instructions

La programmation en assembleur est hors du programme de 1ère NSI. Cependant, le programme stipule de 
comprendre le déroulement de l’exécution d’une séquence d’instructions simples. 

En guise d'exemple, nous allons donc traduire en assembleur les instructions écrites ci-dessous :

```python
a = 3
b = 5
c = a + b
```

Ce programme est ici écrit en langage Python. Le processeur ne comprend pas ce langage : les instructions 
doivent lui être passées en langage-machine. C'est le rôle des interpréteurs (pour le Python, par exemple) 
ou des compilateurs (pour le C, par exemple) que de faire le lien entre le langage pratiqué par les humains 
(Python, C...) et le langage-machine, qui n'est qu'une succession de chiffres binaires.

Par exemple, notre code ci-dessus s'écrit  

```
01010000 00001111 00011000 00000000
00000000 00000000 01010000 00111111
00011100 00000000 00000000 00000000
01100000 00000011 01000000 00111111
00100000 00000000 00000000 00000000
00000000 00000000 00000000 00000000
00000011 00000000 00000000 00000000
00000101 00000000 00000000 00000000
```

en langage-machine.  

 
En assembleur, notre programme s'écrirait (par exemple) :

```
.pos 0
    mrmovl a, %eax
    mrmovl b, %ebx
    addl %eax, %ebx
    rmmovl %ebx, c
    halt

.align 4
a:  .long 3
b:  .long 5
c:  .long 0    
```

- Le [simulateur Y86](https://dept-info.labri.fr/ENSEIGNEMENT/archi/y86js_v2/index.html){. target="_blank"} permet de simuler la manière dont le processeur va exécuter ce programme. 

- Vous pouvez retrouver le programme à charger [ici](res/prog_asm.ys){. target="_blank"}.

![image](res/cap_Y86_2.png){: .center}

#### 3.3 Code en langage-machine :

Sur la partie droite du simulateur, la zone Mémoire contient, après assemblage, la traduction de notre code en langage-machine  :
```
500f1800
0000503f
1c000000
6003403f
20000000
00000000
03000000
05000000
```

Une fois transformé en binaire, on retrouve le code donné au début du paragraphe précédent.

??? note "Ressources sur les instructions Y86"
    ![image](res/instrY86.png){: .center width=30%}
    ![image](res/encodage.png){: .center width=60%}
  
### Exercices

??? question "Exercice 1"
    === "Énoncé"

        Coder en assembleur la séquence d'instruction suivante :


        ```python
        w = 10
        x = 3
        y = 5
        z = w - (x + y)
        ```

        Vous aurez pour cela besoin de l'instruction `subl rA rB` qui effectue l'opération `rB-rA` et la stocke dans `rB`. (`rA` et `rB` sont les noms des registres).

    === "Correction"

        Une suite d'instructions possible est :

        ```
        .pos 0
            mrmovl x, %eax
            mrmovl y, %ebx
            mrmovl z, %ecx
            addl %eax, %ebx
            subl %ebx, %ecx
            rmmovl %ecx, z
            halt

        .align 4
        w:  .long 10
        x:  .long 3
        y:  .long 5
        z:  .long 0
        ```

??? question "Exercice 2"

    Tester avec le simulateur les instructions ci-dessous.

    Que fait ce programme ?

    ```
        mrmovl a,%eax
        iaddl  1,%eax
        rmmovl %eax,a
        halt

        .pos 0x100
        a:  .long 0
    ```
  
??? question "Exercice 3"

    Tester avec le simulateur les instructions ci-dessous.

    Que fait ce programme ?

    Écrire sur votre cahier chaque instruction avec un commentaire expliquant son action

    ```
            mrmovl a,%eax
            mrmovl b,%ebx
            subl   %eax,%ebx # b >= a ?
            jge    else
            irmovl 1,%ecx
            jmp    suite
    else:
            irmovl 2,%ecx
    suite:
            rmmovl %ecx,c
            halt

            .pos 0x100
    a:      
            .long 0
    b:     
            .long 0
    c:     
            .long 0
    ```

??? question "Exercice 4"

    Écrire en langage d'assembleur Y86 un programme qui calcule le reste de la division 
    euclidienne entre deux nombres.
         
??? quote "Bibliographie "
    - Numérique et Sciences Informatiques, Terminale, T. BALABONSKI, S. CONCHON, J.-C. FILLIATRE, K. NGUYEN, éditions ELLIPSES.
    - G.Lassus (lycée François Mauriac, Bordeaux), [Première NSI - Architecture de Von Neumann](https://glassus.github.io/premiere_nsi/T3_Architecture_materielle/3.2_Architecture_Von_Neumann/cours/)
    - D.Roche, [Informatique Lycee - Première](https://pixees.fr/informatiquelycee/prem/c8.html)
    - Prépabac NSI, Terminale, G.CONNAN, V.PETROV, G.ROZSAVOLGYI, L.SIGNAC, éditions HATIER.