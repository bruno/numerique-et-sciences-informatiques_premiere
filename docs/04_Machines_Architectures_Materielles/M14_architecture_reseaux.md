---
author :
title : Architecture réseaux
hide : 
 - footer
---

## 1. Histoire d'Internet

### 1.1 ARPANet

En 1958, en pleine guerre froide, les USA souhaitent garder une constante suprématie en termes 
technologique et scientifique. C'est à cette période, dans ce contexte et dans ce but qu'est créée 
la DARPA (Defense Advanced Research Projects Agency -- Agence pour les projets de recherche 
avancée de défense). Le problème principal rencontré à cette époque est que les grandes 
universités américaines n'ont que très peu de moyens de communication, ce qui ne leur permet pas 
de centraliser leurs recherches. C'est entre 1962 et 1968 qu'un projet d'interconnexion entre les 
ordinateurs de ces grandes universités a été mis en place : **ARPAnet**, qui devint le premier 
réseau informatique à grande échelle de l'histoire. En 1969, le premier message est transmis 
entre 2 universités : le mot "login". Résultat : seules les lettres `l' et `o' sont arrivées à 
destination.

### 1.2 Invention du protocole TCP/IP

En 1972, 23 ordinateurs du monde entier sont connectés à ARPAnet. En parallèle, d'autres réseaux 
sont créés, mais n'ayant pas le même protocole de communication, il est impossible pour des 
ordinateurs de réseaux différents de communiquer entre eux. C'est alors que 2 ingénieurs de la 
DARPA (Vint Cerf et Bob Khan) vont créer le protocole TCP/IP, qui va très rapidement s'imposer 
comme un standard et être adopté par tous les réseaux, dont ARPAnet. Il était donc à ce moment là 
possible d'interconnecter plusieurs réseaux : Internet était né (Internet est un diminutif de 
``internetting" qui signifie ``Connexion entre réseaux"). TCP/IP est encore aujourd'hui le 
protocole utilisé sur Internet.

## 2. Vocabulaire et composants réseaux

Il existe globalement 2 manières de raccorder un ordinateur à un réseau : via un câble Ethernet
ou via le Wi-Fi. Il existe d’autres manières que nous n’évoquerons pas, car ces deux-là sont les 
plus utilisées actuellement (pour en savoir plus, renseignez-vous sur le CPL ou le Li-Fi par exemple).

Un câble **Ethernet** est constitué de 8 fils conducteurs (souvent du cuivre) par paires 
torsadées, possédant à ses 2 extrémités des prises **RJ45**. Ces prises   sont connectées 
à un ordinateur par une interface située sur une carte réseau. 
Le débit de ces câbles Ethernet oscille entre  0Mbits/s et 10Gbits/s chez les particuliers 
(certains datacenters possèdent des câbles Ethernet de 25 à 400 Gbits/s) et ne fonctionnent 
que sur des distances maximales de 100m environ.

!!! exemple inline end "RJ45"

    ![rj45](res/reseaux-rj45.png)

!!! exemple  "carte réseaux"

    ![carte](res/reseaux-carte.png)



 De l’autre côté de l’ordinateur ne se trouve pas un autre ordinateur, il s’agit plutôt en général d’un **commutateur** (ou **switch** en anglais). Ces switchs possèdent plusieurs prises RJ45 (entre 4 et plusieurs centaines selon les besoins). 

Dans le réseau ci-dessous, chaque ordinateur peut communiquer avec chacun des 3 autres grâce au 
switch.

!!! exemple inline end  "réseau de 4 ordinateurs"

    ![reseau4](res/reseaux-4ordis.png)

!!! exemple "switchs à 8, 16 et 24 ports"

    ![carte](res/reseaux-switch.png)



Le nombre de ports d’un commutateur étant limité, il est également possible d’en utiliser
plusieurs pour créer un réseau.

!!! exemple inline end  ""

    ![reseau4](res/reseaux-5ordis.png)

!!! exemple ""

    Dans le réseau ci-contre, les 5 ordinateurs sont en réseau et peuvent communiquer entre eux, 
    bien qu’ils soient connectés sur des commutateurs différents, car ces derniers sont
    connectés entre eux.

Lorsque le réseau n’est pas filaire, il s’agit en général d’un réseau Wi-Fi (signifiant Wireless 
Fidelity, par analogie au Hi-Fi pour High Fidelity) qui fonctionne par ondes radio. L’ordinateur 
doit pour cela posséder une carte wifi. De la même façon que pour un réseau filaire, pour pouvoir
 interconnecter plus de 2 ordinateurs par wifi, il faudra utiliser un concentrateur wifi (ou point
  d’accès).

## 3. IP et routage

### 3.1 Topologie des réseaux

Les réseaux que nous avons évoqués jusqu’à présent sont des réseaux locaux, de type LAN (si-
gnifiant Local Area Network). C’est-à-dire que ces réseaux mettent en jeu des machines qui peuvent directement communiquer entre elles. Ce n’est pas le cas de tous les réseaux. En effet, pour pouvoir aller sur Internet par exemple, une machine appartenant à un LAN doit pouvoir sortir de ce LAN.

Cela se fera grâce à d’autres composants réseaux, appelés routeurs. Les routeurs sont des machines permettant de faire transiter des données entre plusieurs réseaux. Ces données transitent alors au sein d’un autre type de réseau : un WAN (sig 
réseaux (MAN, SAN, . . .) que nous n’évoquerons pas.

Dans un WAN, les données ne transitent pas grâce à des câbles Ethernet, car ces derniers ne sont
pas suffisants pour transporter la quantité de données sur un WAN à la vitesse voulue. On utilise
dans ce cas généralement la fibre optique (ou une liaison satellitaire, mais rarement), transportant les données grâce à la lumière (300 000 km/s) sur des distances définies. Les débits vont alors de 10 Mbits/s à un record de 2017 de 10, 16 pétabits par seconde (1 péta = 1 000 téra = 1 000 000 giga).

!!! question "VPN"

    Et les VPN alors ? Un réseau privé virtuel (Virtual Private Network) est un mécanisme permettant
    à deux ordinateurs ne faisant pas partie d’un même réseau de pouvoir communiquer entre eux comme
    si c’était le cas, et donc de faire comme si un des deux ordinateurs faisait partie du LAN de l’autre.

### 3.2 Adresses IP

Au sein d’un réseau (qu’il soit local ou non), pour qu’une machine puisse communiquer avec une
autre, il lui faut un identifiant. De la même manière que lorsque vous envoyez un courrier, chaque
ordinateur possède une adresse, appelée adresse IP (IP signifie Internet Protocol). Ces adresses
sont composées de 4 octets séparés par des points et généralement écrits en décimal. Étant donné que
chaque partie de l’adresse IP correspond à un octet, les nombres décimaux la composant seront toujours
compris entre 0 et 255 inclus (28 = 256 valeurs possibles). Exemple d’adresse IP : 192.168.8.10

#### Masques 

Chaque adresse IP peut être décomposée en 2 parties : une partie sert à identifier 
le réseau dans lequel se trouve la machine et l’autre partie sert à identifier la machine 
dans ce réseau. Pour savoir à quel endroit de l’adresse se trouve la séparation entre les deux
 parties, on utilise ce que l’on appelle un masque. Ce masque est précisé après l’adresse et 
le caractère ‘/’. 

!!! exemple "Application d'un masque"

    192.168.8.10/24 signifie que le masque est de taille 24, c’est-à-dire que les 24 premiers 
    bits (3 premiers octets) servent à identifier le réseau et les 8 bits (l’octet) restants
    servent à identifier la machine au sein du réseau. Dans  cet exemple, la partie réseau est 
    donc 192.168.8 et la partie machine est 10. 

!!! warning "Autres type de masques"

    En général, la séparation est faite entre 2 octets (donc le nombre derrière le ‘/’ sera 
    8, 16 ou 24), c’est ce que nous étudierons en NSI, mais ce n’est pas toujours le cas. Le jour où vous serez confronté à des masques différents, il faudra repasser l’adresse IP en binaire pour savoir où la séparation est faite.

    Souvent, le masque est représenté d’une autre manière : tous les bits correspondants à la 
    partie réseau sont égaux à 1, et tous les bits correspondants à la partie machine sont égaux
    à 0. Cela donne des masques de la forme 255.255.255.0 dans le cas d’un masque en /24. 
    
    Ces 2 notations sont donc équivalentes.


#### Adresses réservées

Certaines adresses IP ne sont pas disponibles et ne permettent pas de représenter
des machines, car elles sont réservées pour représenter autre chose :

- **0.0.0.0** est l’adresse temporaire d’une machine dont l’adresse IP n’a pas encore
 été configurée. L’attribution initiale d’une adresse peut se faire automatiquement 
 grâce au protocole DHCP, ou manuellement.  
- **255.255.255.255** sert à une machine à demander l’attribution d’une adresse IP via DHCP.
Cette machine ne connaissant rien sur la configuration du réseau dans lequel elle se trouve, elle
ne connaı̂t pas non plus l’adresse de la machine possédant le serveur DHCP. Elle utilise donc
cette adresse 255.255.255.255, appelée broadcast (diffusion).  
- **127.0.0.1** sert à l’auto-identification d’une machine (adresse de bouclage).  
- **[identifiant réseau].0** sert à identifier le réseau lui-même. Dans l’exemple précédent, l’adresse du réseau auquel appartient la machine 192.168.8.10/24 est 192.168.8.0  
- **[identifiant réseau].255** sert à identifier tous les machines d’un même réseau, pour un
broadcast limité aux machines d’un réseau.


#### Épuisement des adresses 

Aujourd’hui, la majorité des réseaux utilise ce que nous avons présenté ci-dessus : 
il s’agit de la version 4 du protocole IP, que l’on nomme IPv4. Cependant, cette manière de 
fonctionner possède une limite : l’épuisement des adresses. En effet, les adresses IP étant représentées sur 32 bits (4 octets), le nombre total d’adresses
disponibles est 232 = 4 294 967 296. Supposons que chaque être humain sur Terre possède une machine
et qu’il souhaite se connecter à Internet (ce qui n'est pas le cas, mais beaucoup possèdent plusieurs machines : ordinateur fixe, portables, à la maison et au bureau ... ). Il n’est donc pas possible d’attribuer une adresse unique à chaque machine.

Pour régler ce problème, plusieurs solutions ont été mises en place :

- À ses débuts, personne n’imaginait qu’Internet devienne incontournable dans le monde entier,
et l’attribution des adresses IP était très généreuse. Ainsi, certaines entreprises disposent de
réseaux en /8, soit 24 bits d’adresses machines disponibles (224 = 16 777 216), ce dont ils
n’avaient pas réellement besoin. Une solution temporaire est donc de récupérer des adresses en
réduisant la taille des réseaux de ces organisations. Mais cela ne réglera pas le problème sur le
long terme... 

- Vous aurez peut-être déjà remarqué que peu importe la situation envisagée, les exemples
d’adresses IP pris commencent tous par 192.168. Cette adresse réseau est en réalité réservée
pour être utilisée par tous les réseaux, en interne. En effet, chaque réseau ne possède en réalité qu’une seule adresse IP publique. Cette adresse est celle de son routeur le reliant à Internet.
Ce dernier étant le seul point de passage entre Internet et un réseau interne, on peut donc
attribuer les adresses IP que l’on souhaite à l’intérieur de ce réseau. Les adresses débutant par
192.168 étant réservées dans ce but, une même adresse de cette forme peut donc se retrouver
dans des réseaux différents. Le principe est le même que pour une adresse postale : un même
nom de rue peut exister dans plusieurs villes, le courrier arrivera tout de même à bon port.

- La solution précédente n’est pas encore suffisante, car de plus en plus de besoins en adresses IP mènent tout de même à l’épuisement d’adresses IP publiques ; le nombre grandissant d’objets
connectés dans notre quotidien n’aide pas à ce phénomène. Une manière de fonctionner tota-
lement différente de celle d’IPv4 a donc été créée : IPv6. Cette version propose des adresses
sur 128 bits (16 octets), représentées en hexadécimal par groupe de 2 octets séparés par des
“deux-points”. Exemple : 2001:0db8:0000:85a3:0000:0000:ac1f:8001

Rien ne dit que l’on n’arrivera pas un jour à l’épuisement de ces adresses (rappel : il était in-
imaginable que l’on dépasse la capacité d’IPv4 à sa création), mais ayant un nombre d’adresses
possibles immense, la marge actuelle est grande.
2128 = 340 282 366 920 938 463 463 374 607 431 768 211 456 ≈ 3, 4 × 1038 .

### 3.3 Communication et routage

Pour pouvoir communiquer sans passer par un routeur, deux machines doivent nécessairement
appartenir au même réseau, c’est-à-dire que la partie réseau de leurs adresses IP doit être identique. Sinon, il sera nécessaire d’effectuer un routage en passant par un routeur. 

!!! example "Exemple complet de réseau"

    ![reseaux-lycee](res/reseaux-lycee.png)

    Le plan ci-dessus est un plan simplifié d’un réseau qui pourrait tout à fait être celui du lycée. Les informations y figurant montrent ceci :

    - L’adresse IP du réseau est 192.168.0.0/16.  
    - Il est composé de 5 sous-réseaux en /24, reliés à l’aide de 2 routeurs.  
    - Les sous-réseaux 192.168.1.0/24 et 192.168.2.0/24 peuvent correspondre à des salles de
    cours, et le sous-réseau. 192.168.0.0/24 peut correspondre à la salle où est gérée le réseau.
    - Deux points d’accès Wi-fi ont également été représentés.  
    - L’adresse IP publique du réseau est 85.169.33.39/8 et une machine sur Internet ayant comme
    adresse 8.8.8.8 (le masque de cette adresse importe peu) est également représentée.  

    Ce sont les deux routeurs qui gèrent le routage. Ils possèdent des adresses IP dans tous les 
    réseaux auxquels ils sont rattachés. Le routage est effectué grâce à des tables de routage, 
    c’est-à-dire des tableaux de correspondance entre des adresses IP de réseau et les interfaces du 
    routeur. Lorsque le routeur de gauche veut envoyer une information à la machine possédant 
    l’adresse 192.168.0.1/24, il sait qu’il doit l’envoyer via son interface possédant l’adresse 
    192.168.5.1/24.

## 4. DNS

Lorsque vous naviguez sur Internet, vous manipulez des noms de domaine (ex : wikipedia.org)
plutôt que des adresses IP. En effet, il est plus facile pour un humain de retenir un nom 
qu’une suite de 4 nombres. Ce mécanisme est fait grâce au protocole DNS (Domain Name Server) 
qui fait correspondre des noms de domaine avec des adresses IP.
