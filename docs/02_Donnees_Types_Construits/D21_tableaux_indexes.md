---
author :
title : Tableaux indexés
hide :
 -  footer
---

Jusqu'à présent nous avons étudié des types de variables de bases, tels que les entiers, 
les flottants ou encore les booléens. En complément de ces types de bases, il existe des 
types dits "construits" qui  comme leur nom l'indique sont des constructions du langage.

Ces types construits permettent de regrouper de manière structurée un ensemble de valeurs.
Il existe différentes sortes de types construits, que l'on utilise selon le type de données
que l'on souhaite stocker et la façon dont on veut les manipuler.

Nous allons dans ce chapitre étudier la construction de type **Tableau indexé** (ou tableau dynamique) qui correspond en Python au type nommé `list`.


!!! warning "Ne pas confondre `list` et liste"

    La dénomination `list` en Python est quelque peu malheureuse car en informatique
    une *liste* désigne une structure de donnée différente du *tableau*.

    La structure de liste sera étudiée en classe de terminale.


## 1. Notion de tableau

Un *Tableau* est un type complexe de données permettant de représenter plusieurs valeurs ordonnées et possiblement répétées dans une même variable.

Un *Tableau* permet donc de stocker dans une seule variable plusieurs valeurs, tout en permettant d'y accéder individuellement par la suite.


!!! note "Déclaration en Python"

    En Python, on construit un tableau en listant ces valeurs entre crochets et séparées 
    par des **virgules**.

    ```python
    >>>tab = [1, 3 ,5 ]
    ```

    Ici on a déclaré une variable `tab` contenant un tableau d'entiers. Les valeurs sont 
    ordonnées, la première est 1, la seconde 3 et la troisième est 5.

    Il est possible de déclarer des tableaux avec d'autres types de données, par exemples :

    ```python
    >>>neveux = ["riri", "fifi" ,"loulou" ]
    ```

    Il est aussi possible de mélanger le type de données, même si la pratique est plutôt
    déconseillée;

    Une liste vide se déclare tout simplement :

    ```python
    >>>vide = []
    ```

!!! info "Organisation en mémoire"

    On peut se figurer un tableau comme une succesion de cases consécutives contenant
    des valeurs. C'est ainsi qu'un tableau est physiquement créé : sous la forme de 
    zones contiguës dans la mémoire.


## 2. Accéder à un élément

Pour accéder à un élément contenu dans le tableau `tab` on utilise la syntaxe `tab[i]`, 
où `i`représente l'**indice** (le numéro de la case) de l'élément souhaité.

!!! warning "Attention au numéro"

    La numérotation d'un tableau commence à l'indice 0. C'est une source d'erreurs
    commune lorsqu'on débute en programmation !


## 3. Taille d'un tableau

!!! info "Obtenir la taille d'un tableau"

    La taille ou longueur d'un tableau est donnée par l'appel de la fonction `len()`
    sur ce tableau :

    ```python
    >>>tab = [1, 3 ,5 ]
    >>>len(tab)
    >>>3
    ```

    ```python
    >>>vide = []
    >>>len(vide)
    >>>0
    ```

!!! warning "Attention au numéro (bis)"

    ![numerobis](res/numerobis.gif)

    Les indices d'un tableau sont donc numérotés de 0 à len-1. Un indice qui dépasse 
    la valeur  longueur du tableau -1 provoquera donc une erreur `list index out of range`.

    ```python
    >>> famille = ["Bart", "Lisa", "Maggie"]
    >>> famille[0]
    'Bart'
    >>> famille[1]
    'Lisa'
    >>> famille[2]
    'Maggie'
    >>> famille[3]
    Traceback (most recent call last):
    File "<pyshell>", line 1, in <module>
    IndexError: list index out of range
    ```
    
    C'est une erreur très fréquente lorsqu'on manipule des tableaux. 


## 4. Parcours d'un tableau

Il est courant que l'on veuille parcourir les éléments d'un tableau, par exemple lorsque
l'on recherche une valeur particulière, ou que l'on souhaite accumuler tout ou une partie des valeurs.

Il existe deux méthodes pour parcourir séquentiellement (l'un après l'autre) tous les éléments d'un tableau. Ces deux méthodes sont à maîtriser impérativement.

### 4.1 Parcours avec une boucle

Chaque élément étant accessible par son indice (de `0` à `len(tableau) - 1` ), il suffit de faire parcourir à une variable `i` l'ensemble des entiers de `0` à `len(liste) - 1`, par l'instruction `range(len(liste))` :

!!! question "Exercice : utilisation de l'indice"

    Écrire ci-dessous le code permettant de déclarer, parcourir et d'afficher les éléments du
    tableau `simpson` qui contient les éléments `Maggie`, `Lisa`, `Bart`, `Marge`, `Homer`.

    **Remarque** : la variable d'indice est usuellement nommée `i`, `j`, `k` ou encore `indice`.

    {{ IDE()}}


### 4.2 Parcours par élément

Il est aussi possible d'itérer directement sur les éléments du tableau, car le type `list`
en python est un type itérable (voir 6.2, du cours sur la boucle `for`). Cette méthode permet
ainsi de se passer de la variable de boucle.

!!! example "Le retour des neveux"

    Par exemple :

    ```python
    neveux = ["riri", "fifi" ,"loulou" ]

    for neveu in neveux :
            print(neveu)
    ```

    va renvoyer en console :

    ```python
    riri
    fifi
    loulou
    ```

    **Remarque** : on voit dans cet exemple qu'il est judicieux de choisir un nom
    explicite pour la variable qui va parcourir la liste.

        
!!! question "Exercice : parcourir un tableau et comparer ses éléments"

    Après un référendum, le tableau `urne` contient uniquement des 'oui' ou des 'non'.  
    **Question** : quel est le vainqueur de ce référendum ?

    Compléter ci-dessous le programme permettant de répondre à la question, en parcourant
    le tableau par élément.

    {{ IDEv ('tableaux')}}


### 4.3 Avantages et inconvénients des différents parcours.

#### Parcours par élément

Les avantages 👍

- la simplicité : un code plus facile à écrire, avec un nom de variable explicite.
- la sécurité : pas de risque d'erreur de type `index out of range`

Les inconvénients 👎

- méthode rudimentaire : lorsqu'on est «positionné» sur un élément, il n'est pas possible
    de connaître sa position, ni d'accéder au précédent ou au suivant (et c'est parfois utile).
- on ne peut pas modifier l'élément (voir partie 5) sur lequel on est positionné :

```python
>>> lst = [1, 2, 3]
>>> for nb in lst:
        nb = nb * 2 
>>> lst 
[1, 2, 3] 
```

#### Parcours par indice

Les avantages 👍

- la maîtrise : en parcourant par indice, on peut s'arrêter où on veut, on peut accéder au suivant/précédent...
- pour les tableaux à deux dimensions (chapitre suivant), on retrouve la désignation classique d'un élément par numéro de ligne / numéro de colonne.

Les inconvénients 👎

- la rigueur demandée : il faut connaître le nombre d'éléments du tableau, et être sûr
de soi lors de la manipulation des indices.


## 5. Modifier les éléments d'un tableau

En Python, les objets de type `List` sont modifiables (on emploie le mot mutables). Et c'est 
souvent une bonne chose, car des tableaux peuvent évoluer après leur création.

Lorsqu'on souhaitera figer le contenu d'un tableau (pour des raisons de sécurité du code essentiellement), on utilisera  alors le type `Tuple`, qui sera vu ultérieurement.


### 5.1 Modification d'un élément existant

Il suffit d'écraser la valeur actuelle avec une nouvelle valeur.

!!! example "Exemple d'écrasement"

    ```python 
    >>> famille = ["Bart", "Lisa", "Maggie"]
    >>> famille[0] = "Bartholomew" # oui, c'est son vrai nom
    >>> famille
    ['Bartholomew', 'Lisa', 'Maggie']       
    ```

### 5.2 Ajout d'un élement à la fin d'un tableau : la méthode **append()**


!!! example "Exemple d'ajout"

    ```python 
    >>> famille = ["Bart", "Lisa", "Maggie"]
    >>> famille.append("Milhouse")
    >>> famille
    ['Bart', 'Lisa', 'Maggie', 'Milhouse']  
    ```

**Remarques :**

- La méthode `append()` rajoute donc un élément **à la fin** de la liste.  
- Dans **beaucoup** d'exercices, on part d'une liste vide ```[]``` que l'on remplit peu à peu
 avec des ```append()```.  
- *(Hors Programme)* Il est possible d'insérer un élément à la position ```i``` avec la méthode ```insert``` :
  
```python
>>> famille = ["Bart", "Lisa", "Maggie"]
>>> famille.insert(1, "Nelson") # on insère à la position 1
>>> famille
['Bart', 'Nelson', 'Lisa', 'Maggie']
```

!!! question  "Exercice : construction avec `append`"

    === "Énoncé"
        Construire en utilisant la méthode `append` un tableau dynamique contenant tous 
        les nombres inférieurs à 100 qui sont divisibles par 7.

    === "Proposition "
        {{ IDE()}}

### 5.3 Suppression d'un élément

#### Méthode remove() 

!!! note "Exemple de suppression"

    ```python 
    >>> famille = ['Bart', 'Nelson', 'Lisa', 'Maggie']
    >>> famille.remove("Nelson")
    >>> famille
    ['Bart', 'Lisa', 'Maggie']
    ```

**Remarques :**

- Attention, ```remove``` n'enlève que la *première occurrence* de l'élément désigné. 
S'il y en a d'autres après, elles resteront dans le tableau :
  
```python
>>> lst = [3, 1, 4, 5, 1, 9, 4]
>>> lst.remove(4)
>>> lst
[3, 1, 5, 1, 9, 4]
```

- Si l'élément à supprimer n'est pas trouvé, un message d'erreur est renvoyé :
  
```python
>>> lst = [3, 1, 4, 5, 1, 9]
>>> lst.remove(2)
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
ValueError: list.remove(x): x not in list
```


####  Instruction ```del```

L'instruction `del`  (qui n'est pas une fonction) permet de supprimer un élément en 
donnant son indice.

```python
>>> maliste = [8, 4, 2, 5, 7]
>>> del maliste[3]
>>> maliste
[8, 4, 2, 7]
```

### 5.4 Manipuler avec précaution !

![oups](https://media.tenor.com/3goXaDkjUwQAAAAd/jenga-fail.gif)

!!! warning "Tableaux et effets de bord"

    Une variable contenant un tableau ne contient pas une succession de 
    valeurs enregistrées dans des cases, mais une *adresse mémoire* associée 
    à ce tableau.

    Ainsi le tableau `t` déclaré ainsi `t=[1,2,3]` que l'on pourrait se 
    représenter ainsi :

    ![tab1](res/tab1.svg){width=100px}

    Contient en fait l'*adresse mémoire* de l'espace alloué au tableau, et il 
    faut plutôt se représenter les choses ainsi :

    ![tab2](res/tab2.svg){width=150px}

    A priori cette précision n'a pas l'air cruciale, mais considérons maintenant 
    la création d'un nouveau tableau `u` déinit par `u = t`. On affecte ainsi 
    a `u` la valeur de la variable `t`, et donc l'adresse mémoire du tableau. 
    Ce que l'on peut représenter de la façon  suivante :

    ![tab3](res/tab3.svg){width=150px}

    Les deux variables `t` et `u` désignent alors le ***même tableau***, ce qui 
    signifie que toute modification sur `t` entrainera une modification de `u` 
    et inversement.


!!! example "Exemple de modifications périlleuses de tableaux"

    ```python
    >>>t=[1,2,3]
    >>>u = t
    >>>u[0] = 5
    >>>t
    [5,2,3]
    >>>t[2]=6
    >>>u
    [5,2,6]
    ```

    Le suivi de l'exécution avec PythonTutor nous permet de voir le déroulement de 
    l'action.

    <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=t%3D%5B1,2,3%5D%0Au%20%3D%20t%0Au%5B0%5D%20%3D%205%0At%5B2%5D%3D6&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>


## 6. Construire de grands tableaux

!!! tips "Opérateur *"

    Si on doit déclarer un tableau de plusieurs centaines d'éléments, il peut être
    compliqué de le faire en les énumérant tous; il serait aussi possible d'utiliser
    une boucle et la méthode append(), mais il y a plus simple.

    Il est possible d'utiliser l'opérateur `*` afin de d'initialiser un tableau (avec
    des zéros par exemple) en dupliquant la même case.

    ```python
    >>> t = [0] * 1000
    ```

        On obtient ici un tableau de taille 1000.

!!! tips "Opérateur +"

        Il est aussi possible de *concaténer* deux tableaux, à savoir construire un tableau
        contenant bout à bout les éléments de deux tableaux.

        ```python
        >>> [1,2,3] + [4,5,6]
        [1,2,3,4,5,6]
        ```

        Il faut bien comprendre que l'on crée un nouveau tableau, le premier tableau n'a pas 
        été modifié (à moins de le demander explicitement).

## 7. Tableaux et chaînes de caractère

Les tableaux et les chaînes de carcatères ont un comportement quelque peu similaire puisque
ces deux types constituent ce qu'on appelle des "séquences".

Ainsi les tableaux et les chaînes sont itérables (on peut les parcourir) et l'on peut accéder 
à un de leur élément avec un indice ou directement. De même, les opérateurs `*`
et `+` fonctionnent identiquement.

```python
>>>c = "Bonjour"
>>>c[0]
B
>>>c*2
BonjourBonjour
```

Néanmoins les chaînes de caractères ont une particularité que l'on rencontrera avec 
d'autre types de séquence : elles sont **immuables**. Il n'est donc pas possible d'en
modifier un élément.

```python
>>>c = "Bonjour"
>>>c[0]='T'
Traceback (most recent call last):
 File "<console>", line 1, in <module>
TypeError: 'str' object does not support item assignment
```



## 7. Utilisation avancée des tableaux

### 7.1. Construction d'un tableau *en compréhension*

La méthode de tableau en compréhension propose une manière élégante, rapide et naturelle 
pour créer des tableaux.

!!! question "En compréhension ?"

    Cette expression vient des mathématiques. On dit qu'on définit un sous-ensemble 
    *par compréhension* lorsqu'on part d'un ensemble plus grand dont on ne garde que 
    les éléments vérifiant une certaine propriété.


On pourrait par exemple définir les élèves de Première NSI de cette manière :

 *«élèves du lycée inscrits en classe de Première ayant choisi la spécialité NSI»*

 On part d'un ensemble large (les élèves du lycée) qu'on va ensuite réduire par des 
caractérisations spécifiques : être un élève de Première, puis avoir choisi la 
spécialité NSI.

!!! example "Premier exemple"

    Imaginons que nous possédons un tableau ```data``` de températures, dont nous ne voulons 
    garder que celles strictement supérieures à 20.

    ```python
    >>> data = [17, 22, 15, 28, 16, 13, 21, 23]
    >>> good = [t for t in data if t > 20]
    >>> good
    [22, 28, 21, 23]
    ```

**Explications :**

![image](res/comp.png){: .center width=50%}

#### 1.2.1 Le filtre éventuel

C'est lui qui donne tout son sens à cette méthode : il permet de ne garder que 
certaines valeurs. Il est pourtant éventuel : que se passe-t-il s'il n'y a pas de filtre ?

```python
>>> data = [17, 22, 15, 28, 16, 13, 21, 23]
>>> good = [t for t in data]
>>> good
[17, 22, 15, 28, 16, 13, 21, 23]
```

On se retrouve évidemment avec un nouveau tableau qui contient exactement les éléments du tableau de départ, ce qui n'est pas très intéressant.

Pourtant les tableaux en compréhension *sans filtre* sont très fréquents, nous le verrons plus loin.

!!! question "Exercice n°1"

    === "Énoncé"
        On considère la variable ```phrase = 'Bonjour les vacances sont finies'``` et la variable ```voyelles = 'aeiouy'```.

        Construire en compréhension le tableau ```liste_voyelles``` qui contient toutes les voyelles présentes dans la variable ```phrase```.   

    === "Correction"

        ```python
        >>> phrase = 'Bonjour les vacances sont finies'
        >>> voyelles = 'aeiouy'
        >>> liste_voyelles = [lettre for lettre in phrase if lettre in voyelles]
        >>> liste_voyelles
        ['o', 'o', 'u', 'e', 'a', 'a', 'e', 'o', 'i', 'i', 'e']
        ```
        


!!! info "l'ensemble de départ"

    C'est à partir de lui que va se construire notre liste. Pour l'instant, cet ensemble de départ a toujours été de type ```list```.

    Cet ensemble peut être aussi donné à partir de l'instruction ```range()```. 

Souvenons-nous de l'exercice : «Construire un tableau contenant tous les nombres inférieurs à 100 qui sont divisibles par 7.».

Une solution possible était :

```python linenums='1'
lst = []
for n in range(1, 101):
    if n % 7 == 0:
        lst.append(n)
```

Ce code peut maintenant s'écrire très simplement en une seule instruction :

!!! note "Exemple à connaître"

    ```python
    >>> lst = [n for n in range(1,101) if n % 7 == 0]
    >>> lst
    [7, 14, 21, 28, 35, 42, 49, 56, 63, 70, 77, 84, 91, 98]
    ```


!!! info "la valeur à garder"

    Pour l'instant, nous avons procédé à des filtres sur des ensembles existants, sans 
    modifier la valeur filtrée (la valeur _à garder_).  

    Le tableaux en compréhension deviennent encore plus intéressants lorsqu'on comprend qu'il est possible de modifier la valeur filtrée :


        ```python
        >>> lst_carres = [t**2 for t in range(1,10)]
        >>> lst_carres
        [1, 4, 9, 16, 25, 36, 49, 64, 81]
        ```

!!! question "Exercice n°2"

    === "Énoncé"
        1. On considère la fonction mathématique $f : x \mapsto 2x+3$. Coder la fonction ```f```.
        2. Créer (en compréhension) un tableau contenant l'image des entiers de 1 à 10 par la fonction $f$.
   
    === "Correction"

        ```python linenums='1'
        def f(x):
            return 2*x + 3

        lst = [f(x) for x in range(1, 11)]
        ```
        

!!! question "Exercice n°3"

    === "Énoncé"
        On considère le tableau ```lst = [51, 52, 66, 91, 92, 82, 65, 53, 86, 42, 79, 95]```. Seuls les nombres entre 60 et 90 ont une signification : ce sont des codes ASCII (récupérables par la fonction ```chr``` ).  
        Créer (en compréhension) un tableau `sol` qui contient les lettres correspondants aux nombres ayant une signification.

    === "Correction"

        {{IDEv()}}

### 7.2. Tableaux à plusieurs dimensions : tableaux de tableaux

Nous avons vu qu'un tableau pouvait contenir des éléments de tous types : des entiers, des chaines des caractères... et pourquoi pas un tableau qui contient des tableaux ?

#### Syntaxe

!!! note "Exemple de tableaux à deux dimensions"

    La liste ```tab``` ci-dessous est composée de 3 listes qui elles-mêmes contiennent trois nombres :
    ```python
    tab =  [[3, 5, 2],
            [7, 1, 4], 
            [8, 6, 9]]
    ```

    - ```tab[0][0] = 3```
    - ```tab[0][1] = 5```
    - ```tab[2][1] = 6``` 
    - ```tab[1] = [7, 1, 4]``` 

    ![image](res/tab2.png){: .center width=30%}
    
Le tableau `a` est composée de 3 éléments qui sont eux-même des tableaux de 3 éléments.


!!! question "Exercice n°4"

    === "Énoncé"
        On considère le jeu du Morpion (ou *Tic-Tac-Toe*) dont la surface de jeu vierge est representée par le tableau :  
        ```tab = [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']]``` 

        Les premiers coups joués sont ceux-ci :

        - ```tab[1][1] = 'X'``` 
        - ```tab[2][1] = 'O'``` 
        - ```tab[2][2] = 'X'``` 
        - ```tab[0][0] = 'O'``` 

        Quel coup doit maintenant jouer le joueur  `'X'` pour s'assurer la victoire ?

    === "Correction"

        ```tab[0][2] = 'X'```

        


#### Parcours d'un tableau de tableaux

!!! note "Exemple de parcours"

    - Parcours par éléments :
    ```python linenums='1'
    for ligne in tab:
        for elt in ligne:
            print(elt)
    ```

    - Parcours par indice :
    ```python linenums='1'
    for i in range(3):
        for j in range(3):
            print(tab[i][j])
    ```

!!! question "Exercice n°5"

    === "Énoncé"
        On considère le tableau ```m``` ('m' comme *matrice*) suivante :  

        ```m = [[1, 9, 4], [4, 1, 8], [7, 10, 1]]```  

        Écrire en pseudo-code et en python le programme permettant de calculer 
        la somme de tous les nombres de la matrice ```m``` ?

    === "Correction"
    





références :

 - T.Balabonski, S.Conchon, JC.Filliâtre, K.Nguyen  *Numérique et Sciences Informatiques*, Ed. Ellipses
 - G.Lassus (lycée François Mauriac, Bordeaux), [Première NSI - Base de Python, Instruction conditionnelle if](https://glassus.github.io/premiere_nsi/T2_Representation_des_donnees/2.1_Listes/cours/)